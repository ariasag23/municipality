import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class News extends StatefulWidget {
  State<StatefulWidget> createState() {
    return NewsState();
  }
}

class NewsState extends State<News> {
  Future allPerson() async {
    var url = "https://gazaapp.000webhostapp.com/news.php";
    var response = await http.get(Uri.parse(url));
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.grey.shade300,
          appBar: AppBar(
            backgroundColor: Colors.teal,
            toolbarHeight: 80.0,
            actions: <Widget>[
              Container(
                  padding: EdgeInsets.only(top: 5.0, left: 15.0, bottom: 7.0),
                  child: Image.asset(
                    "assets/municipality.png",
                  )),
            ],
            title: Container(
                child: Column(children: <Widget>[
              Center(
                child: Text(
                  "بلدية غزة ",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
              ),
              Center(
                child: Text(
                  "municipality of gaza",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ),
            ])),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            ),
          ),
          body: FutureBuilder(
            future: allPerson(),
            builder: (context, snapshot) {
              if (snapshot.hasError) print(snapshot.error);
              return snapshot.hasData
                  ? ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        List list = snapshot.data;

                        return Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          elevation: 4,
                          color: index % 2 == 0 ? Colors.white : null,
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Column(
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15),
                                        topRight: Radius.circular(15),
                                      ),
                                      child: Image.asset(
                                        list[index]['img'],
                                        height: 200,
                                        width: double.infinity,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 20,
                                      right: 10,
                                      child: Container(
                                        width: 300,
                                        color: Colors.black54,
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 20),
                                        child: Text(
                                          list[index]['title'],
                                          style: TextStyle(color: Colors.white),
                                          softWrap: true,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Column(children: [
                                  Container(
                                      margin: EdgeInsets.only(
                                          top: 5, left: 10, right: 10),
                                      child: Text(list[index]['body'])),
                                ])
                              ],
                            ),
                          ),
                        );
                      })
                  : Center(
                      child: CircularProgressIndicator(),
                    );
            },
          ),
        ));
  }
}
