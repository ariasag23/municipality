import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'drawer.dart';

class StaffPhonedata {
  String nameEmployees;
  String circle;
  int phone;
  int jawwal;

  StaffPhonedata({this.nameEmployees, this.circle, this.phone, this.jawwal});

  factory StaffPhonedata.fromJson(Map<String, dynamic> json) {
    return StaffPhonedata(
        nameEmployees: json['nameEmployees'],
        circle: json['circle'],
        phone: json['phone'],
        jawwal: json['jawwal']);
  }
}

class StaffPhones extends StatefulWidget {
  @override
  _StaffPhonesState createState() => _StaffPhonesState();
}

class _StaffPhonesState extends State<StaffPhones> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  bool _isVisible = false;

  TextEditingController _nameEmployeeController = TextEditingController();

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget dividerLine = //
      Padding(
    padding: const EdgeInsets.only(
      top: 8,
      left: 16,
      right: 16,
    ),
    child: Container(
      child: Divider(color: Colors.green),
      height: 0,
    ),
  );
//

  var url = "https://gazaapp.000webhostapp.com/nameEmployees.php";

// ignore: missing_return
  Future<List<StaffPhonedata>> allStaffPhones() async {
    var data = {'nameEmployees': _nameEmployeeController.text};
    print(data);
    var response = await http.post(Uri.parse(url), body: json.encode(data));
    print(response.body);
    if (response.statusCode == 200) {
      showToast();
      final items = json.decode(response.body).cast<Map<String, dynamic>>();

      List<StaffPhonedata> studentList = items.map<StaffPhonedata>((json) {
        return StaffPhonedata.fromJson(json);
      }).toList();

      return studentList;
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا حاول مرة اخرى"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.green,
            title: Center(
              child: Text(
                "هواتف الموظفين",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 26,
                ),
              ),
            ),
            leading:
// action button
                IconButton(
              icon: Icon(Icons.arrow_back_ios_outlined),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          endDrawer: MyDrawer(),
          body: Directionality(
              textDirection: TextDirection.rtl,
              child: ListView(
                children: <Widget>[
                  Card(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin:
                              EdgeInsets.only(top: 20.0, right: 30, left: 30),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: 'اسم الموظف',
                                labelStyle: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.green))),
                            controller: _nameEmployeeController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'ادخل الاسم';
                              }
                              return null;
                            },
                            onSaved: (value) {},
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: RaisedButton(
                            child: Text('بحث'),
                            color: Colors.green,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(16.0))),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                allStaffPhones();
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    child: FutureBuilder<List<StaffPhonedata>>(
                        future: allStaffPhones(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData)
                            return Center(child: CircularProgressIndicator());
                          return Visibility(
                              visible: _isVisible,
                              child: Container(
                                padding:
                                    EdgeInsets.only(top: 10.0, right: 30.0),
                                child: ListView(
                                  children: snapshot.data
                                      .map((data) => Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "الاسم :",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 16),
                                                  ),
                                                  Container(
                                                      padding: EdgeInsets.only(
                                                          right: 20.0),
                                                      child: Text(
                                                          data.nameEmployees
                                                              .toString(),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14))),
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "الدائرة :",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 16),
                                                  ),
                                                  Container(
                                                      padding: EdgeInsets.only(
                                                          right: 20.0),
                                                      child: Text(
                                                          data.circle
                                                              .toString(),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14))),
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "هاتف داخلي :",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 16),
                                                  ),
                                                  Container(
                                                      padding: EdgeInsets.only(
                                                          right: 20.0),
                                                      child: Text(
                                                          data.phone.toString(),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14))),
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "جوال :",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 16),
                                                  ),
                                                  Container(
                                                      padding: EdgeInsets.only(
                                                          right: 20.0),
                                                      child: Text(
                                                          data.jawwal
                                                              .toString(),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14))),
                                                ],
                                              ),
                                              dividerLine
                                            ],
                                          ))
                                      .toList(),
                                ),
                              ));
                        }),
                  ),
                ],
              )),
        ));
  }
}
