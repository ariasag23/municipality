  import 'package:flutter/material.dart';
  import 'drawer.dart';

  class Numbers extends StatefulWidget {
    
    @override
    _NumbersState createState() => _NumbersState();
  }

  class _NumbersState extends State<Numbers>
    
  {

    @override
    Widget build(BuildContext context) {
        Widget dividerLine = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Divider(color: Colors.cyan),height: 0,
      ),);    
      //   
      Widget serviceFigures = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 120,),
      child: Container(child: Center(child :
      Text("هواتف خدمات بلدية غزة", style: TextStyle(color: Colors.cyan,fontSize: 16,
      fontWeight: FontWeight.w900),),),),);
      //     
      Widget mainCentre = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("المركز الرئيسي - ميدان فلسطين :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget mainCentreNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2832200", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget emergency = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("الطوارئ :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget emergencyNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("115", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget water = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("المياه :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget waterNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("155", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget general = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text(" الجباية العامة :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget generalNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2865492", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget inspection = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text(" التفتيش والمتابعة :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget inspectionNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2883949", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget information = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 24,),
      child: Container(
      child: Text("الاعلام :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget informationNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("0599815600", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget health = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 24,),
      child: Container(
      child: Text("الصحة والبيئة :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget healthNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2821270", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget positionNumbers = //
      Padding(padding: const EdgeInsets.only(top: 12, left: 16, right: 90,),
      child: Container(child: Center(child :
      Text("ارقام هواتف مراكز الجباية والتفتيش", style: TextStyle(color: Colors.cyan,fontSize: 16,
      fontWeight: FontWeight.w900),),),),);
      //
      //
      Widget hassanAlBanna = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز حسن البنا :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget hassanAlBannaNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2837161", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget syriaCenter = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز السرايا شروق ارض المعارض :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget syriaCenterNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2861460", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget beachCentre = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز الشاطئ منتزة الشاطئ:", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget beachCentreNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2824963", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget shujaiyaCenter = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز الشجاعية :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget shujaiyaCenterNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2812217", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget alaCenterAlAminCenter = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز الامين :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget alaCenterAlAminCenterNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2641520", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget barcelonaCentre = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز برشلونة :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget barcelonaCentreNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2641521", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget sheikhradwanCenter = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز شيخ رضوان :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget sheikhradwanCenterNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2877587", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
        Widget appleCenter = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("مركز التفاح:", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget appleCenterNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("2868272", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget emergencyNumbers = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 150,),
      child: Container(child: Center(child :
      Text("ارقام الطوارئ", style: TextStyle(color: Colors.cyan,fontSize: 16,
      fontWeight: FontWeight.w900),),),),);
      //   
      Widget palestinianPolice = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("الشرطة الفلسطنية :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget palestinianPoliceNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("100", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //  
      Widget  ambulanceAndEmergency = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text(" الاسعاف والطوارئ :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget ambulanceAndEmergencyNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("101", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //  
      Widget  civilDefence = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text(" الدفاع المدني :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget civilDefenceNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("102", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //  
      Widget  centralOperations = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("العمليات المركزية :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget centralOperationsNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("109", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //  
      Widget  gazaMunicipality = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 16,),
      child: Container(
      child: Text("بلدية غزة :", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //
      Widget gazaMunicipalityNumber = //
      Padding(padding: const EdgeInsets.only(top: 8, left: 16, right: 8,),
      child: Container(
      child: Text("115", style: TextStyle(color: Colors.black,fontSize: 18,
      fontWeight: FontWeight.w400),),),);
      //  
      //
      return 
      Scaffold(
          appBar: AppBar(
      backgroundColor: Colors.cyan,
    
    title: Center
    ( child :
      Text("أرقام الطوارئ",style: TextStyle(color: Colors.white,fontSize: 26,), ),
    ),
      leading: 
              // action button
              IconButton(
                icon: Icon( Icons.arrow_back_ios_outlined ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
        ),
       endDrawer:MyDrawer( 
        ),
        body:  Directionality(
      
      textDirection: TextDirection.rtl,
     child :  
        Container(child:
        Directionality(
        textDirection: TextDirection.rtl,
        child: 
        ListView(children: <Widget>[
        Column(children: <Widget>[
        //  
        Row(children: <Widget>[
        serviceFigures
        ],),

        Divider(color: Colors.cyan,thickness: 3,endIndent: 70,indent: 100,height: 0,),
        //
        Row(children: <Widget>[
        // 
        mainCentre,
        mainCentreNumber,
        // 
        ],),
        //
        dividerLine,
        
      //
        Row(children: <Widget>[
        // 
        emergency,
        emergencyNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        water,
        waterNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        general,
        generalNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        inspection,
        inspectionNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        information,
        informationNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        health,
        healthNumber,
        // 
        ],),
        //
        dividerLine,
        //
        //
        Row(children: <Widget>[
        positionNumbers
        ],),

        Divider(color: Colors.cyan,thickness: 3,endIndent: 50,indent: 85,height: 0,),
        //
        Row(children: <Widget>[
        // 
        hassanAlBanna,
        hassanAlBannaNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        syriaCenter,
        syriaCenterNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        beachCentre,
        beachCentreNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        shujaiyaCenter,
        shujaiyaCenterNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        alaCenterAlAminCenter,
        alaCenterAlAminCenterNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        barcelonaCentre,
        barcelonaCentreNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        sheikhradwanCenter,
        sheikhradwanCenterNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        appleCenter,
        appleCenterNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        emergencyNumbers
        ],),

        Divider(color: Colors.cyan,thickness: 3,endIndent: 110,indent: 130,height: 0,),
        //
        Row(children: <Widget>[
        // 
        palestinianPolice,
        palestinianPoliceNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        ambulanceAndEmergency,
        ambulanceAndEmergencyNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        civilDefence,
        civilDefenceNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        centralOperations,
        centralOperationsNumber,
        // 
        ],),
        //
        dividerLine,
        //
        Row(children: <Widget>[
        // 
        gazaMunicipality,
        gazaMunicipalityNumber,
        // 
        ],),
        //
        dividerLine,
        //
        //
        ],),],),
      ),),),);
    
      
    }
  }


