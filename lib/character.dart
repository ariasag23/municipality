import 'dart:convert';

import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Character extends StatefulWidget {
  @override
  _CharacterState createState() => _CharacterState();
}

class _CharacterState extends State<Character> {
  bool _isVisible = false;
  String getNameCraft;
  String getNameUserCraft;
  String getBuilding;
  String getCity;
  String getPhone;
  String getJawwal;
  bool isCarft = false;

  final GlobalKey<FormState> _formKey = GlobalKey();

  TextEditingController _namecarftcontroller = TextEditingController();
  TextEditingController _nameusercarftcontroller = TextEditingController();
  TextEditingController _buildingcontroller = TextEditingController();
  TextEditingController _citycontroller = TextEditingController();

  Future setCraft(String namecraft, String nameusercraft, String building,
      String city, String phone, String jawwal) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString("namecraft", namecraft);
    await prefs.setString("nameusercraft", nameusercraft);
    await prefs.setString("building", building);
    await prefs.setString("city", city);
    await prefs.setString("phone", phone);
    await prefs.setString("jawwal", jawwal);
  }

  void getCraft() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    getNameCraft = prefs.getString("namecraft");
    getNameUserCraft = prefs.getString("nameusercraft");
    getBuilding = prefs.getString("building");
    getCity = prefs.getString("city");
    getPhone = prefs.getString("phone");
    getJawwal = prefs.getString("jawwal");
    if (getNameCraft != null) {
      setState(() {
        getNameCraft = prefs.getString("namecraft");
        getNameUserCraft = prefs.getString("nameusercraft");
        getBuilding = prefs.getString("building");
        getCity = prefs.getString("city");
        getPhone = prefs.getString("phone");
        getJawwal = prefs.getString("jawwal");
        isCarft = true;
      });
    }
  }

  @override
  void initState() {
    getCraft();
    super.initState();
  }

  craftData() async {
    final response = await http.post(
        Uri.parse("https://gazaapp.000webhostapp.com/craft.php"),
        body: {"namecraft": _namecarftcontroller.text});

    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 1) {
      showToast();
      print(data["namecraft"]);
      setCraft(data['namecraft'], data['nameusercraft'], data['building'],
          data['city'], data['phone'], data['jawwal']);
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا لا يوجد بيانات"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
      print(pesan);
    }
  }

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget dividerLine = //
      Padding(
    padding: const EdgeInsets.only(
      top: 8,
      left: 16,
      right: 16,
    ),
    child: Container(
      child: Divider(color: Colors.blue),
      height: 0,
    ),
  );
//
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Center(
          child: Text(
            "دليل الحرف",
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
            ),
          ),
        ),
        leading:
            // action button
            IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      endDrawer: MyDrawer(),
      body: Form(
        key: _formKey,
        child: Directionality(
            textDirection: TextDirection.rtl,
            child: ListView(children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text(
                      "بحث عن الحرف المجاورة لموقعك على الخريطة",
                      style: TextStyle(color: Colors.cyan),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 30.0, right: 30.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                        labelText: "المسافة بالمتر",
                        suffixIcon: Icon(
                          Icons.gps_fixed_rounded,
                          color: Colors.cyan,
                        ),
                        fillColor: Colors.cyan,
                        border: new OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.cyan),
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20.0, right: 30, left: 30),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        labelText: "اسم الحرف",
                        fillColor: Colors.white,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'ادخل اسم الحرفة';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (value) {},
                      controller: _namecarftcontroller,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.0, right: 30, left: 30),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        labelText: "اسم صاحب الحرفة",
                        fillColor: Colors.white,
                      ),
                      controller: _nameusercarftcontroller,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.0, right: 30, left: 30),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                        labelText: "مبني",
                        fillColor: Colors.white,
                      ),
                      controller: _buildingcontroller,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.0, right: 30, left: 30),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                        labelText: "شارع",
                        fillColor: Colors.white,
                      ),
                      controller: _citycontroller,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: RaisedButton(
                      child: Text('بحث'),
                      color: Colors.cyan,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(16.0))),
                      onPressed: () {
                        {
                          if (_formKey.currentState.validate()) {
                            craftData();
                          } else {
                            return null;
                          }
                        }
                      },
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 30),
                      child: Divider(
                        color: Colors.cyan,
                        thickness: 4,
                        indent: 30,
                        height: 0,
                        endIndent: 30,
                      )),
                  Visibility(
                    visible: _isVisible,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            child: Text(
                              "اسم الحرفة  :",
                              style: TextStyle(
                                  color: Colors.cyan,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
//

                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 8,
                          ),
                          child: Container(
                            child: isCarft
                                ? Text(
                                    getNameCraft,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Text(""),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _isVisible,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            child: Text(
                              "مبنى :",
                              style: TextStyle(
                                  color: Colors.cyan,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 8,
                          ),
                          child: Container(
                            child: isCarft
                                ? Text(
                                    getBuilding,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Text(""),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _isVisible,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            child: Text(
                              "شارع :",
                              style: TextStyle(
                                  color: Colors.cyan,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 8,
                          ),
                          child: Container(
                            child: isCarft
                                ? Text(
                                    getCity,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Text(""),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _isVisible,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            child: Text(
                              "تلفون :",
                              style: TextStyle(
                                  color: Colors.cyan,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 8,
                          ),
                          child: Container(
                            child: isCarft
                                ? Text(
                                    getPhone,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Text(""),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _isVisible,
                    child: Row(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 8,
                          left: 16,
                          right: 18,
                        ),
                        child: Container(
                          child: Text(
                            "جوال :",
                            style: TextStyle(
                                color: Colors.cyan,
                                fontSize: 18,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 8,
                          left: 16,
                          right: 8,
                        ),
                        child: Container(
                          child: isCarft
                              ? Text(
                                  getJawwal,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                )
                              : Text(""),
                        ),
                      ),
                    ]),
                  ),
                  Visibility(visible: _isVisible, child: dividerLine),
                ],
              ),
            ])),
      ),
    );
  }
}
