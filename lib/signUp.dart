import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:aa/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  bool _secureText = true;
  bool _securetext = true;
  bool visible = false;
  String fullName = "";
  String email = "";
  String water = "";
  String jawwal = "";

  TextEditingController _fullNamedController = TextEditingController();
  TextEditingController _emaillController = TextEditingController();
  TextEditingController _passworddController = TextEditingController();
  TextEditingController _waterController = TextEditingController();
  TextEditingController _jawwalController = TextEditingController();

  void setIntoSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString("fullName", _fullNamedController.text);
    await prefs.setString("email", _emaillController.text);
    await prefs.setString("water", _waterController.text);
    await prefs.setString("jawwal", _jawwalController.text);
  }

  @override
  void initState() {
    setIntoSharedPreferences();
    super.initState();
  }

  Future signup() async {
    var url = "https://gazaapp.000webhostapp.com/signUp.php";
    var response = await http.post(Uri.parse(url), body: {
      "fullName": _fullNamedController.text,
      "email": _emaillController.text,
      "password": _passworddController.text,
      "water": _waterController.text,
      "jawwal": _jawwalController.text,
    });
    var data = json.decode(response.body);
    if (data == "Error") {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text(
                "الايميل او رقم اشتراك المياه بالفعل موجود",
                style: TextStyle(fontSize: 15),
              ),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp()));
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading:
// action button
            IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: ListView(children: <Widget>[
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
              Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(15.0, 20.0, 10.0, .0),
                    child: Text('مشترك',
                        style: TextStyle(
                            fontSize: 50.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(16.0, 80.0, 40.0, 0.0),
                    child: Text('جديد',
                        style: TextStyle(
                            fontSize: 50.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(210.0, 80.0, 220.0, 0.0),
                    child: Text('.',
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.green)),
                  )
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: InputDecoration(
                              labelText: 'الاسم رباعي',
                              suffixIcon: Icon(
                                Icons.account_circle_outlined,
                                color: Colors.green,
                              ),
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              // hintText: 'EMAIL',
                              // hintStyle: ,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green))),
                          controller: _fullNamedController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'ادخل الاسم';
                            }
                            return null;
                          },
                          onSaved: (value) {},
                        ),
                        SizedBox(height: 0.0),
                        TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: InputDecoration(
                              labelText: 'الايميل',
                              suffixIcon: Icon(Icons.email_outlined,
                                  color: Colors.green),
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              // hintText: 'EMAIL',
                              // hintStyle: ,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green))),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'ادخل الايميل';
                            }
                            if (!RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                              {
                                return 'الايميل غير صحيح';
                              }
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) {},
                          controller: _emaillController,
                        ),
                        SizedBox(height: 0.0),
                        TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              labelText: 'رقم الجوال',
                              suffixIcon: Icon(Icons.phone_android_outlined,
                                  color: Colors.green),
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              // hintText: 'EMAIL',
                              // hintStyle: ,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green))),
                          controller: _jawwalController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'ادخل رقم الهاتف';
                            }
                            if (value.isEmpty || value.length < 10) {
                              return 'رقم الهاتف غير صحيح';
                            }
                            if (value.isEmpty || value.length > 10) {
                              return 'رقم الهاتف غير صحيح';
                            }
                            return null;
                          },
                          onSaved: (value) {},
                        ),
                        SizedBox(height: 0.0),
                        TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              labelText: 'رقم اشتراك المياه',
                              suffixIcon: Icon(Icons.local_drink_outlined,
                                  color: Colors.green),
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              // hintText: 'EMAIL',
                              // hintStyle: ,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green))),
                          controller: _waterController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'ادخل رقم المياه';
                            }
                            if (value.isEmpty || value.length > 4) {
                              return 'رقم المياه غير صحيح';
                            }
                            if (value.isEmpty || value.length < 4) {
                              return 'رقم المياه غير صحيح';
                            }
                            return null;
                          },
                          onSaved: (value) {},
                        ),
                        SizedBox(height: 0.0),
                        TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: InputDecoration(
                              labelText: 'كلمة السر',
                              suffixIcon: IconButton(
                                icon: Icon(
                                  _secureText
                                      ? Icons.visibility_off_outlined
                                      : Icons.visibility_outlined,
                                  color: Colors.green,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _secureText = !_secureText;
                                  });
                                },
                              ),
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green))),
                          obscureText: _secureText,
                          controller: _passworddController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'ادخل  كلمة السر';
                            }
                            if (value.isEmpty || value.length <= 5) {
                              return 'كلمة السر قصيرة';
                            }
                            return null;
                          },
                          onSaved: (value) {},
                        ),
                        SizedBox(height: 0.0),
                        TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: InputDecoration(
                              labelText: 'تاكيد كلمة السر',
                              suffixIcon: IconButton(
                                icon: Icon(
                                  _securetext
                                      ? Icons.visibility_off_outlined
                                      : Icons.visibility_outlined,
                                  color: Colors.green,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _securetext = !_securetext;
                                  });
                                },
                              ),
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green))),
                          obscureText: _securetext,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'ادخل  كلمة السر';
                            }
                            if (value.isEmpty ||
                                value != _passworddController.text) {
                              return 'كلمة السر غير متطابقة';
                            }
                            return null;
                          },
                          onSaved: (value) {},
                        ),
                        SizedBox(height: 50.0),
                        Container(
                            height: 40.0,
                            child: Material(
                              borderRadius: BorderRadius.circular(20.0),
                              shadowColor: Colors.greenAccent,
                              color: Colors.green,
                              elevation: 7.0,
                              child: GestureDetector(
                                onTap: () {
                                  if (_formKey.currentState.validate()) {
                                    setIntoSharedPreferences();
                                    signup();
                                  } else {
                                    return null;
                                  }
                                },
                                child: Center(
                                  child: Text(
                                    'تسجيل دخول',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat'),
                                  ),
                                ),
                              ),
                            )),
                        SizedBox(height: 20.0),
                      ],
                    ))),
// SizedBox(height: 15.0),
// Row(
//   mainAxisAlignment: MainAxisAlignment.center,
//   children: <Widget>[
//     Text(
//       'New to Spotify?',
//       style: TextStyle(
//         fontFamily: 'Montserrat',
//       ),
//     ),
//     SizedBox(width: 5.0),
//     InkWell(
//       child: Text('Register',
//           style: TextStyle(
//               color: Colors.green,
//               fontFamily: 'Montserrat',
//               fontWeight: FontWeight.bold,
//               decoration: TextDecoration.underline)),
//     )
//   ],
// )
          ])
        ]),
      ),
    );
  }
}
