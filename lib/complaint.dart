import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'drawer.dart';
import 'map.dart';

class Complaint extends StatefulWidget {
  State<StatefulWidget> createState() {
    return ComplaintState();
  }
}

class ComplaintState extends State<Complaint> {
  String _selectedFruit;
  String _selectedCity;
  File _image;
  final picker = ImagePicker();
  String id;

  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id = prefs.getString("id");
    });
  }

  TextEditingController _titlecontroller = TextEditingController();
  TextEditingController _detailscontroller = TextEditingController();
  TextEditingController _idusercontroller = TextEditingController();

  Future uploadComplaint() async {
    final uri = Uri.parse("https://gazaapp.000webhostapp.com/complaintsd.php");
    var request = http.MultipartRequest('POST', uri);
    request.fields['id_user'] = id;
    request.fields['title'] = _titlecontroller.text;
    request.fields['details'] = _detailscontroller.text;
    request.fields['iduser'] = _idusercontroller.text;
    request.fields['typecomplaintsd'] = _selectedFruit;
    request.fields['area'] = _selectedCity;
    var pic = await http.MultipartFile.fromPath("img", _image.path);
    request.files.add(pic);
    var response = await request.send();

    if (response.statusCode == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("تم العملية بنجاح"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا حاول مرة اخرى"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  Future getImage(ImageSource src) async {
// ignore: deprecated_member_use
    final pickedFile = await picker.getImage(source: src);
    setState(() {
      if (picker != null) {
        _image = File(pickedFile.path);
      } else {
        print("No image Selected.");
      }
    });
  }

  static const _fruits = [
    'مياه',
    'تنظيم',
    'صرف صحي',
    'صحة',
    'طرق',
    'حدائق',
    'انارة',
    'غير ذلك',
    'مالية',
    'شؤون ثقافة',
    'شؤون ادارية',
  ];
  static const _city = [
    'تل الهوى',
    'مدينة العودة',
    'الشيخ رضوان',
    'البلدة القديمة',
    'التفاح',
    'الدرج',
    'الزيتون',
    'التركمان',
    'الصبرة',
    'مخيم الشاطئ',
    'النصر',
  ];
  @override
  void initState() {
    getFromSharedPreferences();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.pink,
      ),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pink,
          title: Center(
            child: Text(
              "تقديم شكوي",
              style: TextStyle(
                color: Colors.white,
                fontSize: 26,
              ),
            ),
          ),
          leading:
              // action button
              IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        endDrawer: MyDrawer(),
        body: Directionality(
          textDirection: TextDirection.rtl,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 30.0, left: 12.0, right: 12.0),
                child: TextFormField(
                  autocorrect: true,
                  decoration: InputDecoration(
                    hintText: 'عنوان الشكوى',
                  ),
                  controller: _titlecontroller,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0, left: 12.0, right: 12.0),
                child: TextFormField(
                  autocorrect: true,
                  maxLines: 3,
                  decoration: InputDecoration(
                    hintText: 'نص الشكوى',
                  ),
                  controller: _detailscontroller,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0, left: 12.0, right: 12.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  autocorrect: true,
                  decoration: InputDecoration(
                    hintText: 'رقم الهوية',
                  ),
                  controller: _idusercontroller,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0, left: 12.0, right: 12.0),
                child: DropdownButton(
                  hint: Text("نوع الشكوى"),
                  isExpanded: true,
                  value:
                      _selectedFruit, // A global variable used to keep track of the selection
                  items: _fruits.map((item) {
                    return DropdownMenuItem(
                      value: item,
                      child: Text(item),
                    );
                  }).toList(),
                  onChanged: (selectedItem) => setState(
                    () => _selectedFruit = selectedItem,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0, left: 12.0, right: 12.0),
                child: DropdownButton(
                  hint: Text("اسم المنطقة"),
                  isExpanded: true,
                  value:
                      _selectedCity, // A global variable used to keep track of the selection
                  items: _city.map((item) {
                    return DropdownMenuItem(
                      value: item,
                      child: Text(item),
                    );
                  }).toList(),
                  onChanged: (selectedItem) => setState(
                    () => _selectedCity = selectedItem,
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => Maps()));
                },
                child: Container(
                    margin: EdgeInsets.only(
                      top: 10.0,
                      left: 100.0,
                    ),
                    child: Row(children: <Widget>[
                      Container(
                        padding:
                            EdgeInsets.only(top: 10.0, left: 0.0, right: 40.0),
                        child: Image.asset(
                          "assets/googlemaps.png",
                          width: 50.0,
                          height: 50.0,
                        ),
                      ),
                      Center(
                        child: Container(
                            padding: EdgeInsets.only(
                                top: 0.0, left: 0.0, right: 10.0),
                            child: Text(
                              "تحديد موقع الشكوى على الخريطة",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 12),
                              textAlign: TextAlign.center,
                            )),
                      )
                    ])),
              ),
              InkWell(
                onTap: () {
                  var ad = AlertDialog(
                    title: Text("Select"),
                    content: Container(
                      height: 135,
                      child: Column(
                        children: [
                          Divider(
                              color: Colors.pink, height: 10.0, thickness: 2),
                          Container(
                            width: 300,
                            child: ListTile(
                                leading: Icon(Icons.image_outlined),
                                title: Text("الاستوديو"),
                                onTap: () {
                                  getImage(ImageSource.gallery);
                                  Navigator.of(context).pop();
                                }),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Divider(
                            color: Colors.pink,
                            height: 0.0,
                          ),
                          Container(
                            width: 300,
                            child: ListTile(
                              leading: Icon(Icons.add_a_photo_outlined),
                              title: Text("الكاميرا"),
                              onTap: () {
                                getImage(ImageSource.camera);
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                  showDialog(builder: (context) => ad, context: context);
                },
                child: Container(
                  margin: EdgeInsets.only(top: 10.0, left: 0.0, right: 40.0),
                  child: Row(children: [
                    Container(
                      child: Image.asset(
                        "assets/gallery.png",
                        width: 50.0,
                        height: 50.0,
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(0.0, 0.0, 40.0, 0.0),
                        child: _image == null
                            ? Text(
                                "صورة الشكوي",
                                style: TextStyle(fontWeight: FontWeight.w600),
                              )
                            : Image.file(
                                _image,
                                width: 80,
                                height: 80,
                              )),
                  ]),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 40, left: 30.0, right: 30.0),
                child: RaisedButton(
                  child: Text(
                    'ارسل',
                    style: TextStyle(fontSize: 24),
                  ),
                  color: Colors.pink,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(16.0))),
                  onPressed: () {
                    uploadComplaint();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
