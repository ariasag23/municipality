  import 'package:flutter/material.dart';
  import 'character.dart';
  import 'because.dart';
  import 'numbers.dart';
  import 'table.dart';


  class  Dalily extends StatefulWidget {
    State <StatefulWidget> createState()
    {
      return DalilyState();
    }
  }

  class DalilyState extends State<Dalily>  {
    @override
    Widget build(BuildContext context) { 
      return Scaffold(
        appBar: AppBar(
      backgroundColor: Colors.cyan,
  
          title : Text("دليلي",style: TextStyle(color: Colors.white,fontSize: 26,)
          , textAlign: TextAlign.center), 
        

            leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
      ),
              
        ),
        

        body:  GridView(
          padding: EdgeInsets.only(right: 10.0, left: 10.0, top: 100.0),
         
          children: <Widget>[
              Card(
                color:Colors.cyan,
                child: InkWell(
              onTap: () {
                  Navigator.push(context,MaterialPageRoute(builder: (context)=>Because()));
              },   
              splashColor: Colors.cyan.shade400,
              child: Center(
                child:Column (
                  mainAxisSize:  MainAxisSize.min,
                  children: <Widget>[
                    Text("دليل الاجراءات", style: new TextStyle(color: Colors.white,fontSize:24),),
                    Icon(Icons.article_outlined,size:50.0,color:Colors.white),
                    
                  ],
                  ),
                ), 
              ),

                ),
                  Card(
                color: Colors.cyan,
                child: InkWell(
              onTap: () {
                Navigator.push(context,MaterialPageRoute(builder: (context)=>Character()));
              },   
              splashColor: Colors.cyan.shade400,
              child: Center(
                child:Column (
                  mainAxisSize:  MainAxisSize.min,
                  children: <Widget>[
                    Text("دليل الحرف", style: new TextStyle(color: Colors.white,fontSize:24),),
                    Icon(Icons.handyman_outlined,size:50.0,color:Colors.white),
                    
                  ],
                  ),
                ), 
              ),

                ),
                Card(
                color:  Colors.cyan,
                child: InkWell(
              onTap: () {
                  Navigator.push(context,MaterialPageRoute(builder: (context)=>Tables()));
              },   
              splashColor: Colors.cyan.shade400,
              child: Center(
                child:Column (
                  mainAxisSize:  MainAxisSize.min,
                  children: <Widget>[
                    Text( "جدول توزيع المياه", style: new TextStyle(color: Colors.white,fontSize:24),),
                    Icon(Icons.table_chart_sharp,size:50.0,color:Colors.white),
                    
                  ],
                  ),
                ), 
              ),

                ),
                Card(
                  color: Colors.cyan,
                
                child: InkWell(
              onTap: () {
                  Navigator.push(context,MaterialPageRoute(builder: (context)=>Numbers()));
              },   
              splashColor: Colors.cyan.shade400,
              child: Center(
                child:Column (
                  
                  mainAxisSize:  MainAxisSize.min,
                  children: <Widget>[
                    Text("ارقام الطوارىء", style: new TextStyle(color: Colors.white,fontSize:24),),
                    Icon(Icons.phone_android_outlined,size:50.0,color:Colors.white,),
                    
                  ],
                  ),
                ), 
              ),

                ),
            ],
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            
            childAspectRatio: 2 / 2
             )
          ),
        
      );
    }
    }