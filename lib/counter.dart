import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'drawer.dart';

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  File _image;
  final picker = ImagePicker();

  final GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _numbercountercontroller = TextEditingController();

  Future getImage(ImageSource src) async {
// ignore: deprecated_member_use
    final pickedFile = await picker.getImage(source: src);
    setState(() {
      if (picker != null) {
        _image = File(pickedFile.path);
      } else {
        print("No image Selected.");
      }
    });
  }

  bool checkBoxValue = false;

  Future uploadCounter() async {
    final uri = Uri.parse("https://gazaapp.000webhostapp.com/counter.php");
    var request = http.MultipartRequest('POST', uri);
    request.fields['numbercounter'] = _numbercountercontroller.text;
    var pic = await http.MultipartFile.fromPath("img", _image.path);
    request.files.add(pic);
    var response = await request.send();

    if (response.statusCode == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("تم العملية بنجاح"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا حاول مرة اخرى"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF4682B4),
        title: Center(
          child: Text(
            "ادخال قراءة العداد",
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
            ),
          ),
        ),
        leading:
// action button
            IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      endDrawer: MyDrawer(),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 150.0, left: 12.0, right: 12.0),
                child: TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                    labelText: "ادخال اول خمسة ارقام من اليسار",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                  ),
                  controller: _numbercountercontroller,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'ادخل رقم العداد';
                    }
                    if (value.isEmpty || value.length > 5) {
                      return 'ادخل خمس ارقام';
                    }
                    if (value.isEmpty || value.length < 5) {
                      return 'ادخل خمس ارقام';
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, left: 12.0, right: 12.0),
                child: TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                    labelText: "تاكيد القراءة",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'ادخل رقم العداد';
                    }
                    if (value.isEmpty ||
                        value != _numbercountercontroller.text) {
                      return 'رقم العداد غير متطابقة';
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, left: 140.0),
                child: CheckboxListTile(
                  title: Text("تحديد الموقع"),
                  secondary: Icon(
                    Icons.gps_fixed_outlined,
                    color: Color(0xFF4682B4),
                  ),
                  controlAffinity: ListTileControlAffinity.platform,
                  value: checkBoxValue,
                  onChanged: (bool value) {
                    setState(() {
                      checkBoxValue = value;
                    });
                  },
                  activeColor: Color(0xFF4682B4),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10.0,
                  left: 150.0,
                ),
                child: Row(children: [
                  Container(
                    child: IconButton(
                      icon: Icon(
                        Icons.add_a_photo_outlined,
                        color: Color(0xFF4682B4),
                        size: 60.0,
                      ),
                      onPressed: () {
                        var ad = AlertDialog(
                          title: Text("Select"),
                          content: Container(
                            height: 135,
                            child: Column(
                              children: [
                                Divider(
                                    color: Color(0xFF4682B4),
                                    height: 10.0,
                                    thickness: 2),
                                Container(
                                  width: 300,
                                  child: ListTile(
                                      leading: Icon(Icons.image_outlined),
                                      title: Text("الاستوديو"),
                                      onTap: () {
                                        getImage(ImageSource.gallery);
                                        Navigator.of(context).pop();
                                      }),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Divider(
                                  color: Color(0xFF4682B4),
                                  height: 0.0,
                                ),
                                Container(
                                  width: 300,
                                  child: ListTile(
                                    leading: Icon(Icons.add_a_photo_outlined),
                                    title: Text("الكاميرا"),
                                    onTap: () {
                                      getImage(ImageSource.camera);
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                        showDialog(builder: (context) => ad, context: context);
                      },
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 80.0, 0.0),
                      child: _image == null
                          ? Text(
                              "صورة للعداد",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            )
                          : Image.file(
                              _image,
                              width: 80,
                              height: 80,
                            )),
                ]),
              ),
              Container(
                margin: EdgeInsets.only(top: 40, left: 30.0, right: 30.0),
                child: RaisedButton(
                  child: Text(
                    'ارسل',
                    style: TextStyle(fontSize: 24),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(16.0))),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      uploadCounter();
                    } else {
                      return null;
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
