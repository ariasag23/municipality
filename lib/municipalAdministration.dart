import 'package:flutter/material.dart';
import 'drawer.dart';
class MunicipalAdministration extends StatefulWidget{

 @override

 State<StatefulWidget> createState() {

   // TODO: implement createState 

   return MunicipalAdministrationState();

 }

 
}

class MunicipalAdministrationState extends State<MunicipalAdministration>{

  List<bool>listExpans=List();

 
  @override

 void initState() {

   // TODO: implement initState

   super.initState();

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
  
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
   
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
 }

 @override

 Widget build(BuildContext context) {

   // TODO: implement build

   return 
    Scaffold(

    

     appBar: AppBar(
       backgroundColor: Colors.green,
       title: Center ( child : Text("المجلس البلدي", )),
       
        leading: 
            // action button
            IconButton(
              icon: Icon( Icons.arrow_back_ios_outlined ),
              onPressed: () {
                Navigator.pop(context);
               },
            ),
      ),
      endDrawer:MyDrawer( 
      ),
       
       
      

     body: Directionality(
      
      textDirection: TextDirection.rtl,
     child :   ListView(

       children : <Widget>[
          Container(

         alignment: Alignment.center,

         child: Column(

           children: <Widget>[

               Container(
           
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
          child: Theme(
            
            data: Theme.of(context).copyWith(cardColor: Colors.green),
            child: 
            

             ExpansionPanelList(

               children : <ExpansionPanel>[

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Padding(
                       padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0,0.0),
                       child :
                      ListTile(

                       title: Text('1. مكتب رئيس البلدية',style: TextStyle(color : Colors.white, fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[
 
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يعمل على استقبال الجمهور والتعامل مع كافة المشاكل التي تعترض طريقهم والعمل على حلها'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('وينقسم مكتب رئاسة البلدية الى دوائر عدة, وهي : '),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('1- دائرة العلاقات العامة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('2- وحدة التعاون الدولي'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('3- دائرةالرقابة العامة والجودة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('4- وحدة المستشار القانوني'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('5- الدائرة القانونية '),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('6- دائرة خدمات الجمهور'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('7- مكتب المجلس البلدي'),),

                         ),Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('8- دائرة الارشيف والسكرتارية'),),

                         ),


                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[0],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('2. الادرة العامة للمياه والصرف الصحي',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('تشرف على قطاعي المياه والصرف الصحي في مدينة غزة, وتعمل على تحديث وتطوير الشبكات وانشاء محطات لمعالجة الصرف الصحي, ووصل كافة المنازل بالشبكة, وحفر ابار جديدة, وايصال المياه الي كافة منازل المواطنين.'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('وتنقسم الادارة العامة للمياه والصرف الصحي الي 3 دوائر, وهي : '),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة المياه'),),

                         ),
                          Card(
                           color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة الصرف الصحي'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة الصيانة الكهروميكانيكية'),),

                         ),
                           Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة محطة المعلجة واعادة الاستخدام'),),

                         ),
                         

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[1],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('3. الادارة العامة للحاسوب وتكنولوجيا المعلومات',style: TextStyle(color : Colors.white, fontSize: 16)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('تعمل على حوسبة كافة معاملات البلدية ولاسيما الاجراءات الادارية والمالية الى جانب حوسبة نظم المعلومات الجغرافية حوسبة اننظمة عمل التخطيط الخضري  GIS'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('وتنقسم الادارة العامة للحاسوب وتكنولوجيا المعلومات الي 4 دوائر,وهي : '),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة البرامج المالية والادارية'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة برامج خدمات البلدية'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة نظم المعلومات الجغرافية'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة الانظمة والشبكات'),),

                         ),
                         
                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[2],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('4. الادارة العامة للشؤون الادارية ',style: TextStyle(color: Colors.white, fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('تختص بكافة الاعمال الادارية داخل بلدية غزة, من متابعة التزام الموظفين بمواعيد العمل وتنفيذ القرارات الصادرة عن رئيس واعضاء المجلس البلدي والمدراء العامون والمسؤولين, واعتماد اجراءات التعيين للموظفين الجدد حسب حاجة العمل.'),),

                         ),
                          Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('وتنقسم الادارة العامة للشؤون الادارية الي 3 دوائر, هي : '),),

                         ),
                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة شؤون المظفين'),),

                         ),
                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة المشتريات والمخازن'),),

                         ),
                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة الخدمات والحراسات'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[3],

                   canTapOnHeader: true,

                 ),

                   ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('5. الادارة العامة للخدمات',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin: EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('تعمل على تنفيذ السياسات والنظم واللوائح المتعلقة بالامور الماليية والتحصيلات في بلدية غزة, وتقوم باعداد مشروع الموازنة التقديرية العامة للبلدية سنويا.'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('وتنقسم الادارة العامة للشؤون المالية الي دائرتين, وهما : '),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('الدائرة المالية'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة التفتيش والمتابعة'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[4],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('6. الادارة العامة للهندسة والتخطيط',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يختص عملها في تصميم المشاريع والاشراف عليها ووضع خطط التطوير للمدينة وتجنيد التمويل للمشاريع وتجميل المدينة والحفاظ على ارثها الثقافي وتحديد طبيعة المناطق داخل المدينة وصيانة الطرق والارصفة والمرور, وتنفيذ المشاريع لحيوية دخل المدينة. '),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('وتنقسم الادارة العامة للهندسة والتخطيط الي 3 دوائر, وهي : '),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة اعداد وتطوير المشاريع'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة التنسيق والاشراف على المشاريع'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('دائرة صيانة الطرق والمنشات'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[5],

                   canTapOnHeader: true,

                 ),
 
               ],

               expansionCallback:(panelIndex, isExpanded){

                 setState(() {

                   listExpans[panelIndex] = !isExpanded;

                 });

               },

               animationDuration : kThemeAnimationDuration,

             ),
          ),
               ),
           ],

         ),

       ),
       ],
     )
    ),
   );

 
 }

 
}

