import 'dart:convert';

import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;

class Tables extends StatefulWidget {
  State<StatefulWidget> createState() {
    return TablesState();
  }
}

class TablesState extends State<Tables> {
  Future allTableData() async {
    var url = "https://gazaapp.000webhostapp.com/table.php";
    var response = await http.get(Uri.parse(url));
    return json.decode(response.body);
  }

  Widget dividerLine = //
      Padding(
    padding: const EdgeInsets.only(
      top: 8,
      left: 16,
      right: 16,
    ),
    child: Container(
      child: Divider(color: Colors.cyan),
      height: 0,
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Center(
          child: Text(
            "جدول توزيع المياه",
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
            ),
          ),
        ),
        leading:
// action button
            IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      endDrawer: MyDrawer(),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "المنطقة",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.cyan,
                              fontWeight: FontWeight.bold,
                              fontSize: 24),
                        ),
                      ),
                      Expanded(
                        child: Text("المكان",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.cyan,
                                fontWeight: FontWeight.bold,
                                fontSize: 24)),
                      ),
                      Expanded(
                        child: Text("التوقيت",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.cyan,
                                fontWeight: FontWeight.bold,
                                fontSize: 24)),
                      ),
                    ],
                  ),
                ),
              ),
              dividerLine,
              Expanded(
                flex: 8,
                child: Container(
                  child: FutureBuilder(
                      future: allTableData(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              List list = snapshot.data;
                              return Card(
                                elevation: 0,
                                margin: EdgeInsets.all(7),
                                child: Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        list[index]['place'],
                                        textAlign: TextAlign.center,
                                      )),
                                      Expanded(
                                          child: Text(
                                        list[index]['area'],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 14),
                                      )),
                                      Expanded(
                                          child: Text(
                                        list[index]['timer'],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(fontSize: 12),
                                      )),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        } else {
                          return Center(child: CircularProgressIndicator());
                        }
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
