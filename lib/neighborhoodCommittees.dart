import 'package:flutter/material.dart';
import 'drawer.dart';
class NeighborhoodCommittees extends StatefulWidget{

 @override

 State<StatefulWidget> createState() {

   // TODO: implement createState 

   return NeighborhoodCommitteesState();

 }

 
}

class NeighborhoodCommitteesState extends State<NeighborhoodCommittees>{

  List<bool>listExpans=List();

 
  @override

 void initState() {

   // TODO: implement initState

   super.initState();

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
  
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
   
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
 }

 @override

 Widget build(BuildContext context) {

   // TODO: implement build

   return  
    Scaffold(

    

     appBar: AppBar(
       backgroundColor: Colors.green,
       title: Center ( child : Text("لجان الاحياء", )),
       
        leading:
            // action button
            IconButton(
              icon: Icon( Icons.arrow_back_ios_outlined ),
              onPressed: () {
                Navigator.pop(context);
               },
            ),
  
      ),
      endDrawer:MyDrawer( 
      ),
       
       
      

     body: Directionality(
      
      textDirection: TextDirection.rtl,
     child :   ListView(

       children : <Widget>[
          Container(

         alignment: Alignment.center,

         child: Column(

           children: <Widget>[
               Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child : Card(
                 color : Colors.white,
                 margin:EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Padding(padding: EdgeInsets.all(8),child: Text('تعد لجان الاحياء هي حلقة الوصل بين المواطن والبلدية, وتلعب دورا بارزا في تقديم المقترحات الخاصة بتطوير الاحياء وبنيتها التحتية, واحتياجات هذة الاحياء من المشاريع, ورفع البلدية, والمساهمة في حل المشاكل المختلفة التي يتعلق بتنظيم الابنية والمتأخرات المالية, ويوجد في مدينة غز 12 لجنة حي مختلفة وهي :',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),),
               ),
               ),

               Container(
           
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
          child: Theme(
            
            data: Theme.of(context).copyWith(cardColor: Colors.green),
            child: 
            

             ExpansionPanelList(

               children : <ExpansionPanel>[

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Padding(
                       padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0,0.0),
                       child :
                      ListTile(

                       title: Text('لجنة حي منطقة الشيخ رضوان',style: TextStyle(color : Colors.white, fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[
 
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('11 عضوا'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('د. محمد نمر المدهون'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599486277'),),

                         ),

                       ],

                     ),

                   ),

                   isExpanded: listExpans[0],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الزيتون',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('11 عضوا'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('د. عبد الكريم الدهشان'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0597665889'),),

                         ),
                    

                       ],

                     ),

                   ),

                   isExpanded: listExpans[1],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الشاط الجنوبي',style: TextStyle(color : Colors.white, fontSize: 16)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('11 عضوا '),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('عبد الكريم ابراهيم المنكوش'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0598121470'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[2],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الشاظئ الشمالي وحي النصر الغربي',style: TextStyle(color: Colors.white, fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('9 اعضاء'),),

                         ),
                          Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('عدنان علي ابو عودة'),),

                         ),
                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599750375'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[3],

                   canTapOnHeader: true,

                 ),

                   ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي المنطقة المحطة - التفاح الغربي',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin: EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('11 عضوا '),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('جمال بدوي البراوي'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0598881878'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[4],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الدرج',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('9 اعضاء'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('سلامة حسن الصفدي'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599602827'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[5],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي المنطقة الشعف - التفاح الشرقي',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('11 عضوا'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('علاء صبحي البطش'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0598502288'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[6],

                   canTapOnHeader: true,

                 ),
                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة تل الاسلام',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('13 عضوا'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('سمير على محمد قنيطة'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599544888'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[7],

                   canTapOnHeader: true,

                 ),
                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الصبرة',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('13 عضوا'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('د. وليد حسن عويضة'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599166307'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[8],

                   canTapOnHeader: true,

                 ),
                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الرمال الشمالي',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('13 عضوا'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('عماد محمد الاعرج'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599526026'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[9],

                   canTapOnHeader: true,

                 ),
                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة الشجاعية',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('8 اعضاء'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاني رائد اسليم'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599527660'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[10],

                   canTapOnHeader: true,

                 ),
                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة حي منطقة المنارة',style: TextStyle(color : Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('13 عضوا'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('د. احمد ابو حلبية'),),

                         ),
                         Card(
                          color : Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('0599607830'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[11],

                   canTapOnHeader: true,

                 ),
 
               ],

               expansionCallback:(panelIndex, isExpanded){

                 setState(() {

                   listExpans[panelIndex] = !isExpanded;

                 });

               },

               animationDuration : kThemeAnimationDuration,

             ),
          ),
               ),
           ],

         ),

       ),
       ],
     )
    ),
   );

 
 }

 
}

