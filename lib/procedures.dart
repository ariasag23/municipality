import 'package:flutter/material.dart';





class  Procedures extends StatefulWidget {
  State <StatefulWidget> createState()
  {
    return ProceduresState();
  }
}

class ProceduresState extends State<Procedures>  {
  final countries = [
      "خدمات البناء",
      "خدمات المياه",
      "خدمات الكهرباء",
      "خدمات الصرف الصحي",
      "خدمات الحرف",
      "خدمات الصحة والبيئة",
      "خدمات الانارة",
      "خدمات الهندسة",
      "خدمات اخرى",
  ];

  @override
  Widget build(BuildContext context) { 
    return Directionality(
      textDirection: TextDirection.rtl,
     child :  
    Scaffold(
  
    body: ListView.separated(
      itemCount : countries.length ,
      itemBuilder: (context , index)
      {
        return ListTile
        (
                 
             title: new Text(countries[index]),
             onTap: () {},
        );
      },
      separatorBuilder: (context , index)
      {
           return  Divider(color: Colors.cyan,thickness: 1,endIndent: 5,indent: 5,height: 0,);
      },

    )  
       
      ),
    );
    
    
    
  }
  }