import 'package:flutter/material.dart';
import 'drawer.dart';
class MunicipalShop extends StatefulWidget{

 @override

 State<StatefulWidget> createState() {

   // TODO: implement createState

   return MunicipalShopState();

 }

 
}

class MunicipalShopState extends State<MunicipalShop>{

  List<bool>listExpans=List();

 
  @override

 void initState() {

   // TODO: implement initState

   super.initState();

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
  
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
   
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
 }

 @override

 Widget build(BuildContext context) {

   // TODO: implement build

   return  
    Scaffold(

    

     appBar: AppBar(
       backgroundColor: Colors.green,
       title: Center ( child : Text("المجلس البلدي", )),
       
        leading:
            // action button
            IconButton(
              icon: Icon( Icons.arrow_back_ios_outlined ),
              onPressed: () {
                Navigator.pop(context);
               },
            ),
          
      ),
      endDrawer:MyDrawer( 
      ),
       
       
      

     body: Directionality(
      
      textDirection: TextDirection.rtl,
     child :   ListView(

       children : <Widget>[
          Container(
          
         child: Column(

           children: <Widget>[
             Container(
           
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
          child: Theme(
            
            data: Theme.of(context).copyWith(cardColor: Colors.green),
            child:

             ExpansionPanelList(

               children : <ExpansionPanel>[
                 
            
                 ExpansionPanel(

 
                   headerBuilder:(context , isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),       
                      child :
                      ListTile(
                      
                       title: Text('المجلس البلدي',style: TextStyle(color : Colors.white, fontSize: 24)),
                      ),
                     
                     );
                     
                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يشكل المجلس البلدي الحالي لمدينة غزة, من 11 عضوا نشيطا يحملون شهادات ودرجات عالية في تخصصات متنوعة, يمثلون احياء ومناطق المدينة, ولديهم الرغبة والدافعية للنهوض بمدينة غزة , ويعملون المجلس البلدي على تحديد السياسات التي تحكم عمل البلدية, ويوفر الاشراف والمساءلة من خلال لجان متخصصة للمجلس البلدي وهناك متابعة من قبل رئيس البلدية ونائب الرئيس الي جانب عقد اجتماعات دورية مع المدراء العاملون.'),),

                         ),

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[0],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('اعضاء المجلس البلدي',style: TextStyle(color: Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                         color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('1. د.يحيى رشدي السراج    رئيس بلدية غزة'),),

                         ),
                          Card(
                           color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('2. م.احمد عبد المنعم ابو راس   عضو المجلس البلدي'),),

                         ),
                          Card(
                               color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('3. م.هاشم عرفة سكيك    عضو المجلس البلدي'),),

                         ),
                          Card(
                            color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('4. د.جميل سليمان طرزي    عضو المجلس البلدي'),),

                         ),
                          Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('5. م.اسماعيل جمال حمادة    عضو المجلس البلدي'),),

                         ),
                           Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('6. أ.تامر رضوان الريس    عضو المجلس البلدي'),),

                         ),
                          Card(
                              color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('7. أ.مروان فارس الفول    عضو المجلس البلدي'),),

                         ),
                         Card(
                        color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('8. أ.مصطفى رشدي قزعاط    عضوالمجلس البلدي'),),

                         ),
                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('9. أ.بدر يونس صبرة   عضو المجلس البلدي'),),

                         ),
                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('10. أ.فداء محسن المدهون   عضو المجلس البلدي'),),

                         ),

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[1],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('اللجان المتخصصة',style: TextStyle(color : Colors.white, fontSize: 24)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                        color: Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('ينبق عن المجلس البلدي مجموعة من اللجان المتخصصة التي تعمل على الدراسة قضايا المواطنين والمدينة واصدار القرارات المتعلقة في الملفات المختلفة, ةتجتمع هذه اللجان بشكل دوري في غرفة المجلس البلدي بمقر البلدية الرئيس, وهذه اللجان هي :'),),

                         ),
                         
                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[2],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة التطوير الاداري',style: TextStyle(color: Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                          Card(
                         color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('د.يحيى السراج'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[3],

                   canTapOnHeader: true,

                 ),

                   ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنه التواصل المجتمعي',style: TextStyle(color: Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                        color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('مصطفى قزعاط'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[4],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنه الاعلام والتوعية',style: TextStyle(color: Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                         color: Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('مروان الغول'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[5],

                   canTapOnHeader: true,

                 ),
                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة المراكز الثقافة',style: TextStyle(color: Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                              color: Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('م.هاشم سكيك'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[6],

                   canTapOnHeader: true,

                 ),
                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة التطوير الالكتروني',style: TextStyle(color: Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                         color: Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('م.اسماعيل حمادة'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[7],

                   canTapOnHeader: true,

                 ),
                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('لجنة المشاريع والاستثمار',style: TextStyle(color: Colors.white,fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                           color: Colors.white,
                            margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('بدر صبرة'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[8],

                   canTapOnHeader: true,

                 ),
                ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('السيرة الذاتية لرئيس البلدية',style: TextStyle(color : Colors.white, fontSize: 24)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                        color : Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 8, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text(''),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[9],

                   canTapOnHeader: true,

                 ),

 
               ],

               expansionCallback:(panelIndex, isExpanded){

                 setState(() {

                   listExpans[panelIndex] = !isExpanded;

                 });

               },

               animationDuration : kThemeAnimationDuration,

             ),
          ),
             ),
           ],

         ),

       ),
       ],
     )
    ),
   );

 
 }

 
}