import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class  Maps extends StatefulWidget {
State <StatefulWidget> createState()
{
return MapState();
}
}

class MapState extends State<Maps>  {
 GoogleMapController  _mapController;
 Set<Marker> _markers = HashSet<Marker>();
 void _onMapCreated(GoogleMapController controller){
   _mapController = controller ;
 setState(() {
   
 
   _markers.add(
     Marker(
  infoWindow: InfoWindow(title:('موقع الشكوى')),   
  markerId : MarkerId('1'),
  position: LatLng(31.529179, 34.459175)
  ),
  );
  });  

 }
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(31.529179, 34.459175),
    zoom: 14.4746,
  );




 @override
  void initState() {
   
    super.initState();
  }
@override
Widget build(BuildContext context) { 

return  Theme(  
data: Theme.of(context).copyWith(
primaryColor: Colors.pink,
),
child : Directionality(

textDirection: TextDirection.rtl,
child : 

Scaffold(
  appBar: AppBar(
backgroundColor: Colors.pink,    
title: Center
( child :
Text("موقع الشكوى",style: TextStyle(color: Colors.white,fontSize: 26,), ),
),
leading: 
  // action button
  IconButton(
    icon: Icon( Icons.arrow_back_ios_outlined ),
    onPressed: () {
      Navigator.pop(context);
      },
  ),
        actions: <Widget>[
          Center( 
            child :
          Text('حفظ',style: TextStyle(fontSize: 20.0,),),),
          IconButton(
            icon: Icon(

              Icons.save,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          
        ],
      ),
           body: Stack(
        children : [
       GoogleMap(
         
        mapType: MapType.normal,
        markers: _markers,
        initialCameraPosition: _kGooglePlex,
        onMapCreated : _onMapCreated,
      ),
         
        ]
      ),
),
)
);

}
}