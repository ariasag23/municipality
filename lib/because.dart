import 'package:flutter/material.dart';
import 'drawer.dart';
import 'procedures.dart';
import 'lrregularities.dart';
import 'centres.dart';


class  Because extends StatefulWidget {
  State <StatefulWidget> createState()
  {
    return BecauseState();
  }
}

class BecauseState extends State<Because>  {
  @override
  Widget build(BuildContext context) { 
    return  Directionality(
      
      textDirection: TextDirection.rtl,
     child : 
    DefaultTabController(  
   length: 3,  
     child :  
    Scaffold(
      appBar: AppBar(
     backgroundColor: Colors.cyan,
     elevation:0,
         title: Center
         ( 
           child :
          Text("دليل",style: TextStyle(color: Colors.white,fontSize: 26,), ),
         ),
       
     
       bottom:  new PreferredSize(
            preferredSize: new Size(55.0, 55.0),
            
            child: new Container(
              
              child: new
        TabBar(
        
         labelColor: Colors.cyan ,
          unselectedLabelColor: Colors.white,
           indicatorSize: TabBarIndicatorSize.label,
        indicator: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft : Radius.circular(10), topRight:Radius.circular(10)),
          color: Colors.white,
          ),
          tabs: [
            Tab(
              child : Align
            (
              alignment: Alignment.center,
               child :
              Text ( "دليل الاجراءات")
              ),
            ),

             Tab(
              child : Align
            (
              alignment: Alignment.center,
               child :
              Text ( "دليل المخالفات")
              ),
            ),

             Tab(
              child : Align
            (
              alignment: Alignment.center,
               child :
              Text ( "المراكز")
              ),
            ),
          ],
       ),
            ),
       ),
     
     actions :[
        IconButton(
              icon: Icon( Icons.arrow_forward_ios_outlined ),
              onPressed: () {
                Navigator.pop(context);
               },
     
            ),
     ]
      ),
             
     
      drawer:MyDrawer(),

     body: TabBarView(
       children: [
         Procedures(),
         Lrregularities(),
         Centres(),
       ],
     ),     
    ),
    ),
    );
  }
  }