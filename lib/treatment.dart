import 'dart:convert';

import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Treatment extends StatefulWidget {
  @override
  _TreatmentState createState() => _TreatmentState();
}

class _TreatmentState extends State<Treatment> {
  bool _isVisible = false;
  String getName;
  String getSender;
  String getFuturs;
  bool isDeals = false;

  final GlobalKey<FormState> _formKey = GlobalKey();

  TextEditingController _numbercontroller = TextEditingController();

  TextEditingController _yearcontroller = TextEditingController();

  Future setDeals(String name, String sender, String future) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString("name", name);
    await prefs.setString("sender", sender);
    await prefs.setString("future", future);
  }

  void getDeals() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    getName = prefs.getString("name");
    getSender = prefs.getString("sender");
    getFuturs = prefs.getString("future");
    if (getName != null) {
      setState(() {
        getName = prefs.getString("name");
        getSender = prefs.getString("sender");
        getFuturs = prefs.getString("future");
        isDeals = true;
      });
    }
  }

  @override
  void initState() {
    getDeals();
    super.initState();
  }

  dealsData() async {
    final response = await http.post(
        Uri.parse("https://gazaapp.000webhostapp.com/deals.php"),
        body: {"number": _numbercontroller.text, "year": _yearcontroller.text});
    final data = jsonDecode(response.body);
    print(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 1) {
      showToast();
      print(data["name"]);
      setDeals(data['name'], data['sender'], data['future']);
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا لا يوجد بيانات"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
      print(pesan);
    }
  }

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget dividerLine = //
      Padding(
    padding: const EdgeInsets.only(
      top: 8,
      left: 16,
      right: 16,
    ),
    child: Container(
      child: Divider(color: Colors.blue),
      height: 0,
    ),
  );
//
  Widget build(BuildContext context) {
//
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF4682B4),
        title: Center(
          child: Text(
            "تتبع المعاملات",
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
            ),
          ),
        ),
        leading:
            // action button
            IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      endDrawer: MyDrawer(),
      body: Form(
        key: _formKey,
        child: Directionality(
            textDirection: TextDirection.rtl,
            child: ListView(children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 20.0, right: 30, left: 30),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                        labelText: "رقم المعاملة",
                        fillColor: Colors.white,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'ادخل رقم المعاملة';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (value) {},
                      controller: _numbercontroller,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.0, right: 30, left: 30),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                        labelText: "السنة",
                        fillColor: Colors.white,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'ادخل السنة';
                        } else {
                          return null;
                        }
                      },
                      onSaved: (value) {},
                      controller: _yearcontroller,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: RaisedButton(
                      child: Text('بحث'),
                      color: Color(0xFF4682B4),
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(16.0))),
                      onPressed: () {
                        {
                          if (_formKey.currentState.validate()) {
                            dealsData();
                          } else {
                            return null;
                          }
                        }
                      },
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 30),
                      child: Divider(
                        color: Color(0xFF4682B4),
                        thickness: 4,
                        indent: 30,
                        height: 0,
                        endIndent: 30,
                      )),
                  Visibility(
                    visible: _isVisible,
                    child: Column(children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(top: 20),
                          child: Center(
                              child: isDeals
                                  ? Text(
                                      getName,
                                      style: TextStyle(
                                          color: Color(0xFF4682B4),
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20),
                                    )
                                  : Text(""))),
                    ]),
                  ),
                  Visibility(
                    visible: _isVisible,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            child: Text(
                              "المرسل  :",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 8,
                          ),
                          child: Container(
                            child: isDeals
                                ? Text(
                                    getSender,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Text(""),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _isVisible,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 16,
                          ),
                          child: Container(
                            child: Text(
                              "المستقبل :",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8,
                            left: 16,
                            right: 8,
                          ),
                          child: Container(
                            child: isDeals
                                ? Text(
                                    getFuturs,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Text(""),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(visible: _isVisible, child: dividerLine),
                ],
              ),
            ])),
      ),
    );
  }
}
