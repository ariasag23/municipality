import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'drawer.dart';
import 'expansionWidget.dart';
import 'municipalShop.dart';
import 'municipalAdministration.dart';
import 'facilitiesAndBuildings.dart';
import 'neighborhoodCommittees.dart';
import 'staffPhones.dart';
import 'package:url_launcher/url_launcher.dart';
import 'numbers.dart';

class About extends StatefulWidget {
  State<StatefulWidget> createState() {
    return AboutState();
  }
}

class AboutState extends State<About> {
  var flicker = "https://www.flickr.com/photos/132398423@N02/";
  var facebook = "https://www.facebook.com/mogaza2";
  var instagram = "https://instagram.com/mogaza1?igshid=13pvrat5hkndd";
  var youtube = "https://www.youtube.com/user/municgaza";
  var twitter = "https://twitter.com/municgaza?s=08";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("عن البلدية",
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
            ),
            textAlign: TextAlign.center),
        leading: IconButton(
          icon: Icon(
            Icons.menu,
          ),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
        ),
      ),
      drawer: MyDrawer(),
      body: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.assignment_late_rounded, color: Colors.green),
            title: Text(
              'عن البلدية',
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            ),
          ),
          ListTile(
            leading: Icon(Icons.fiber_manual_record, color: Colors.green),
            title: Text('البلدية في سطور'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ExpansionWidget()));
            },
          ),
          ListTile(
            leading: Icon(Icons.fiber_manual_record, color: Colors.green),
            title: Text('المجلس البلدي'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => MunicipalShop()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.fiber_manual_record,
              color: Colors.green,
            ),
            title: Text('ادارة البلدية'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          MunicipalAdministration()));
            },
          ),
          ListTile(
            leading: Icon(Icons.fiber_manual_record, color: Colors.green),
            title: Text('مرافق و مباني البلدية'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          FacilitiesAndBuildings()));
            },
          ),
          ListTile(
            leading: Icon(Icons.fiber_manual_record, color: Colors.green),
            title: Text('لجان الاحياء'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          NeighborhoodCommittees()));
            },
          ),
          ListTile(
            leading: Icon(Icons.fiber_manual_record, color: Colors.green),
            title: Text('هواتف الموظفين'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => StaffPhones()));
            },
          ),
          Divider(
              color: Colors.green,
              thickness: 1,
              endIndent: 20,
              indent: 20,
              height: 0),
          ListTile(
            leading: Icon(Icons.add_ic_call_outlined, color: Colors.green),
            title: Text(
              'اتصل بنا',
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              // Update the state of the app.
              // ...
            },
          ),
          Divider(
              color: Colors.green,
              thickness: 1,
              endIndent: 20,
              indent: 20,
              height: 0),
          ListTile(
            leading: Icon(Icons.phone_in_talk_outlined, color: Colors.green),
            title: Text(
              'ارقام تهمك',
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => Numbers()));
            },
          ),
          Divider(
              color: Colors.green,
              thickness: 1,
              endIndent: 20,
              indent: 20,
              height: 0),
          ListTile(
            leading: Icon(Icons.share_outlined, color: Colors.green),
            title: Text(
              'مواقع التواصل ',
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            ),
          ),
          ListTile(
            leading: Image.asset("icons/facebook.png"),
            title: Text('فيس بوك'),
            onTap: () {
              launch(facebook);
            },
          ),
          ListTile(
            leading: Image.asset("icons/twitter.png"),
            title: Text('تويتر'),
            onTap: () {
              launch(twitter);
            },
          ),
          ListTile(
            leading: Image.asset("icons/flickr.png"),
            title: Text('فليكر'),
            onTap: () {
              launch(flicker);
            },
          ),
          ListTile(
            leading: Image.asset("icons/instagram.png"),
            title: Text('انستجرام'),
            onTap: () {
              launch(instagram);
            },
          ),
          ListTile(
            leading: Image.asset("icons/youtube.png"),
            title: Text('يوتيوب'),
            onTap: () {
              launch(youtube);
            },
          ),
        ],
      ),
    );
  }
}
