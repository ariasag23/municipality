import 'package:flutter/material.dart';
import 'drawer.dart';
class ExpansionWidget extends StatefulWidget{

 @override

 State<StatefulWidget> createState() {

   // TODO: implement createState

   return ExpansionWidgetState();

 }

 
}

class ExpansionWidgetState extends State<ExpansionWidget>{

  List<bool>listExpans=List();

 
  @override

 void initState() {

   // TODO: implement initState

   super.initState();

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
  
   listExpans.add(false);

   listExpans.add(false);
 }

 @override

 Widget build(BuildContext context) {

   // TODO: implement build

   return
    Scaffold(

    

     appBar: AppBar(
       backgroundColor: Colors.green,
       title: Center ( child : Text("عن البلدية", )),
       
        leading: 
            // action button
            IconButton(
              icon: Icon( Icons.arrow_back_ios_outlined ),
              onPressed: () {
                Navigator.pop(context);
               },
            ),
      ),
      endDrawer:MyDrawer( 
      ),
       
       
      

     body:  Directionality(
      textDirection: TextDirection.rtl,
     child :ListView(

       children : <Widget>[
          Container(

         alignment: Alignment.center,

         child: Column(

           children: <Widget>[
               Container(
           
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
          child: Theme(
            
            data: Theme.of(context).copyWith(cardColor: Colors.green),
            child:

             ExpansionPanelList(

               children : <ExpansionPanel>[

                 ExpansionPanel(

                   headerBuilder:(context, isExpanded){

                     return  Container(
                       padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                       child :
                     ListTile(
         
                       title: Text("البلدية في سطور",style: TextStyle(color : Colors.white, fontSize: 18),),
                     ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('تعد بلدية غزة اكبر بلديات فلسطين من حيث المساحة وعدد متلقي خدماتها, حيث تقدم خدماتها لما يزيد عن 700 الف وتعتبر مدينة غزة الساحلية والتي تقع قي الطرف الجنوبي للساحل الشرقي من البحر المتوسط هي العاصمة الاقليمية لقطاع غزة البالغ 2 مليون نسمة وتبعد مدينة غزة البالغ مساحتها 56كم2 , عن القدس مسافة 78 كم الي الجنوب الغربي, وهي مركز محافظة غزة واكبر مدن دولة فلسطين من حيث تعدد السكان , ومن اكبر المدن دولة في العالم كثافة سكانية. وانشئت بلدية غزة عام 1893,وتوالت على المدينة محالس بلدية غزة عدة بدات في عهد الدولة العثمانية مرورا بفترةالانتداب البريطاني تم الادارة المصرية لقطاع غزة وبعدها الاحتلال الاسرائيلي, وذلك قبل اصدار الرئيس الراحل ياسر عرفات, قرارا عام 1994م, بتكليف الراحل عون سعدي الشوا, بتشكيل مجلس بلدي جديد لادارة شؤون البلدية.'),),

                         ),
                         
                       ],

                     ),

                   ),

                   isExpanded: listExpans[0],

                   canTapOnHeader: true,

                 ),

               

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(

                       title: Text('رؤية بلدية غزة',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('الرؤية التنموية لمدينة غزة مدينة حيوية مبدعة ومركزا اقتصاديا وحضاريا'),),

                         ),

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[1],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('اهداف بلدية غزة',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text(' *  توفير خدمات حياتية افضل'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تشجيع المشاركة العشعبية في ادارة العمل'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text(' * اتخاذ القرار مع اعطاء اهمية لمواضيع الصحة العامة والبيئة'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text(' * خلق جو صحي للعمل والاستقرار الوظيفي'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text(' * تشجيع المشروعات التنموية و التطويرية'),),

                         ),

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[2],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('سياسة البلدية',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* العدالة و المساواة في توزيع الخدمات'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تشجيع المشاركة المجتمعية وتطوير العلاقة مع سكان المدينة'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* اتباع سياسة المسؤولين وتقييم الاداء للعاملين في اجهزة البلدية مع مكافاة المجتهد ومحاسبة المخطئ'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تقديم خدمات للجمهورية في كافة المجالات الثقافية والاجتماعية والرياضية'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تطوير المدينة القديمة والحفاظ على طابعها الحضاري والثقافي والتاريخي')),
                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تكافؤ الفرص عند التعيين والترفية')),
                         ),

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[3],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text(' اهم الخدمات الاساسية التي تقدمها البلدية',style: TextStyle(color : Colors.white,fontSize: 16,fontWeight: FontWeight.w400)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* اقامة مشاريع تنموية للمدينة'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* جمع وترحيل ومعالجة النفايات ومكافحة القوارض والباعوض')),
                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* توصيل مياه الشرب للمواطنين وحفر الابار وتمديد شبكات المياه'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* معالجة مياه الصرف الصحي تمديد شبكات الصراف الصحي'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* شق وتعبيد وصيانة الطرق وانارة الشوارع')),
                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تنظيم وتلزيم الاسواق وفحص ومراقبة الاغذية')),
                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* ادارة المكتبات والحدائق والمتنزهات والمراكز الثقافية')),
                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* تقديم خدمات ثقافية واجتماعية ورياضية للمواطنين')),
                         ),
                           Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* اعمال الطوارئ لمواجهة الازمات')),
                         ),

                        

                       ],

                     ),

                   ),

                   isExpanded: listExpans[4],

                   canTapOnHeader: true,

                 ),




 
               ],

               expansionCallback:(panelIndex, isExpanded){

                 setState(() {

                   listExpans[panelIndex] = !isExpanded;

                 });

               },

               animationDuration : kThemeAnimationDuration,

             ),
          ),
               ),
           ],

         ),

       ),
       ],
     )
    ),
   );

 
 }

 
}