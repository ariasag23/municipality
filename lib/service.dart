import 'package:flutter/material.dart';
import 'drawer.dart';
import 'treatment.dart';
import 'counter.dart';
import 'care.dart';
import 'bill.dart';

class  Service extends StatefulWidget {
  State <StatefulWidget> createState()
  {
    return ServiceState();
  }
}

class ServiceState extends State<Service>  {

  
  @override
  Widget build(BuildContext context) { 
    return Scaffold(
     
     
      appBar: AppBar(
     backgroundColor: Color(0xFF4682B4),
   
   title: Text("خدماتي",style: TextStyle(color: Colors.white,fontSize: 26,), ),
     
     leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
     ),
      ),
  
      
      drawer:MyDrawer( 
      ),
      
       body:
         
          GridView(
          padding: EdgeInsets.only(right: 10.0, left: 10.0, top: 100.0),
         
          children: <Widget>[
             Card(
               color: Color(0xFF4682B4),
              child: InkWell(
             onTap: () {
              Navigator.push(context,MaterialPageRoute(builder: (context)=>Treatment()));
             },   
             splashColor: Colors.blue.shade300,
             child: Center(
               child:Column (
                 mainAxisSize:  MainAxisSize.min,
                 children: <Widget>[
                   Text("متابعة معاملة", style: new TextStyle(color: Colors.white,fontSize:24),),
                   Icon(Icons.accessibility_new_outlined,size:50.0,color:Colors.white),
                   
                 ],
                 ),
               ), 
             ),

              ),
                Card(
               color:Color(0xFF4682B4),
              child: InkWell(
             onTap: () {
                 Navigator.push(context,MaterialPageRoute(builder: (context)=>Counter()));
             },   
             splashColor: Colors.blue.shade300,
             child: Center(
               child:Column (
                 mainAxisSize:  MainAxisSize.min,
                 children: <Widget>[
                   Text("ادخال قراءة", style: new TextStyle(color: Colors.white,fontSize:24),),
                   Icon(Icons.article_outlined,size:50.0,color:Colors.white),
                   
                 ],
                 ),
               ), 
             ),

              ),
               Card(
               color: Color(0xFF4682B4),
              child: InkWell(
             onTap: () {
               Navigator.push(context,MaterialPageRoute(builder: (context)=>Bill()));
             },   
             splashColor: Colors.blue.shade300,
             child: Center(
               child:Column (
                 mainAxisSize:  MainAxisSize.min,
                 children: <Widget>[
                   Text( "فاتورتي", style: new TextStyle(color: Colors.white,fontSize:24),),
                   Icon(Icons.web_outlined,size:50.0,color:Colors.white),
                   
                 ],
                 ),
               ), 
             ),

              ),
               Card(
                 color: Color(0xFF4682B4),
               
              child: InkWell(
             onTap: () {
                 Navigator.push(context,MaterialPageRoute(builder: (context)=>Care()));
             },   
             splashColor: Colors.blue.shade300,
             child: Center(
               child:Column (
                 
                 mainAxisSize:  MainAxisSize.min,
                 children: <Widget>[
                   Text("احتضان ورعاية", style: new TextStyle(color: Colors.white,fontSize:24),),
                   Icon(Icons.assignment_turned_in_outlined,size:50.0,color:Colors.white,),
                  
                 ],
                 ),
               ), 
             ),

              ),
          ],
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            
            childAspectRatio: 2 / 2
             )
         ),
      

        );
  }
  }
     
   