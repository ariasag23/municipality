import 'package:flutter/material.dart';
import 'main.dart';
import 'treatment.dart';
import 'news.dart';
import 'editprofile.dart';
import 'counter.dart';
import 'bill.dart';
import 'complaint.dart';
import 'because.dart';
import 'numbers.dart';
import 'character.dart';
import 'signIn.dart';
import 'myComplaint.dart';
import 'package:shared_preferences/shared_preferences.dart';


class  MyDrawer extends StatefulWidget {
  State <StatefulWidget> createState()
  {
    return MyDrawerState();
  }
}

class MyDrawerState extends State<MyDrawer>  {
  String email ;
  bool isSingIn = false ;

  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     email = prefs.getString("fullName");
      if(email != null){
    setState(() {
      
      email = prefs.getString("fullName");
      isSingIn = true ;
    });
      }
  }
  @override
  void initState() {
   getFromSharedPreferences();
    super.initState();
  }
  @override
  Widget build(BuildContext context) { 
    
    return  
     Directionality(
      
      textDirection: TextDirection.rtl,
     child : 
     Drawer(
       
      child: Container(color: Color(0xFF303030),
  // Add a ListView to the drawer. This ensures the user can scroll
  // through the options in the drawer if there isn't enough vertical
  // space to fit everything.
  child: ListView(
    // Important: Remove any padding from the ListView.
    padding: EdgeInsets.zero,
    children: <Widget>[
      DrawerHeader(
        child: Image.asset('assets/municipality.png'),
        
        decoration: BoxDecoration(
          color: Color(0xFF303030),
        ),
      ),
      ListTile(
        leading: Icon(Icons.edit,color: Colors.white,),
        title: isSingIn ? Text(email,style: TextStyle(color: Colors.white)) : Text (""),
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>EditeProfile()));
        },
      ),
       Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),
      ListTile(
        leading: Icon(Icons.home,color: Colors.white),
        title: Text('الرئيسية',style: TextStyle(color: Colors.white)),
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>MyHomePage()));
        },
      ),
       Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),

      ListTile(
        leading: Icon(Icons.campaign_outlined,color: Colors.white),
        title: Text('اخر الاخبار',style: TextStyle(color: Colors.white)),
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>News()));
        },
      ),
      Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),

      ListTile(
        leading: Icon(Icons.backup_table_sharp,color: Colors.white),
        title: Text('الاعلانات',style: TextStyle(color: Colors.white)),
        onTap: () {
          // Update the state of the app.
          // ...
        },
      ),
       Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),
       ListTile(
        leading: Icon(Icons.design_services,color: Colors.white),
        title: Text('خدماتي',style: TextStyle(color: Colors.white)),
       
      ),
   
      ListTile(
        title: Text('متابعة المعاملات',style: TextStyle(color: Colors.white)),
        onTap: () {
        Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>Treatment()));
        },
      ),
      ListTile(
        title: Text('فاتورتي',style: TextStyle(color: Colors.white)),
        onTap: () {
            Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => Bill()));
        },
      ),
      ListTile(
        title: Text('ادخال قراءة المداد',style: TextStyle(color: Colors.white)),
        onTap: () {
           Navigator.push(context,MaterialPageRoute(builder: (context)=>Counter()));
        },
      ),
       Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),
        ListTile(
        leading: Icon(Icons.arrow_right_alt,color: Colors.white),
        title: Text('دليلي',style: TextStyle(color: Colors.white)),
        
      ),
      ListTile(
        title: Text('دليل الاجراءات',style: TextStyle(color: Colors.white)),
        onTap: () {
           Navigator.push(context,MaterialPageRoute(builder: (context)=>Because()));
        },
      ),
      ListTile(
        title: Text('دليل الحرف',style: TextStyle(color: Colors.white)),
        onTap: () {
           Navigator.push(context,MaterialPageRoute(builder: (context)=>Character()));
        },
      ),
      ListTile(
        title: Text('ارقام الطوارىء',style: TextStyle(color: Colors.white)),
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (context)=>Numbers()));
        },
      ),
       Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),
          ListTile(
        leading: Icon(Icons.announcement,color: Colors.white),
        title: Text('شكاوي واستفسارات',style: TextStyle(color: Colors.white)),
        
      ),
      ListTile(
        title: Text('تقديم شكوى',style: TextStyle(color: Colors.white)),
        onTap: () {
           Navigator.push(context,MaterialPageRoute(builder: (context)=>Complaint()));
        },
      ),
      ListTile(
        title: Text('عرض الشكاوي والردود',style: TextStyle(color: Colors.white)),
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>MyComplaint()));
        },
      ),
       Divider(color: Colors.white,thickness: 1,endIndent: 50,indent: 5,height: 0,),
        isSingIn ?  ListTile(
        leading: Icon(Icons.login,color: Colors.white),
        title: Text('تسجيل خروج',style: TextStyle(color: Colors.white)),
        onTap: () async{
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.clear();
           Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>SignIn()));
        },
      ) : ListTile(
        leading: Icon(Icons.login,color: Colors.white),
        title: Text('تسجيل دخول',style: TextStyle(color: Colors.white)),
        onTap: () {
           Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) =>SignIn()));
        },
      ) 
    ],
  ),
     ),
     ),
);

  }
}
