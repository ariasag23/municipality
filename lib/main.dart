
import 'dart:ui';
import 'package:aa/drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'news.dart';
import 'service.dart';
import 'dalily.dart';
import 'Inquiries.dart';
import 'about.dart';
void main() {
  runApp(
   MyApp()
  );
}



class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      
      debugShowCheckedModeBanner: false,
      title: 'Municipality',
      
      home: MyHomePage(),
      
    );
  }
}

class MyHomePage extends StatefulWidget {
 

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

   int _currentIndex = 0;
  
   final List <Widget> _children = [
           News(),
           Service(),
           Dalily(),
           Inquiries(),
           About(),
   ];

   void onTappedBar (int index){
     setState(() {
       _currentIndex = index ;
     });
   }
  

   @override
  void initState() {
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   
    return Directionality(
      textDirection: TextDirection.rtl,
     child :
     DefaultTabController(
      length: 5,
    child :Scaffold(
       
      drawer: MyDrawer(),
    
        body: _children[_currentIndex],
        
        
       bottomNavigationBar: BottomNavigationBar(
         onTap:  onTappedBar,
         currentIndex: _currentIndex,
         items: [
           
           BottomNavigationBarItem(
             icon: Icon(Icons.home),
             // ignore: deprecated_member_use 
             title:Text("الرئيسية"),
             backgroundColor: Colors.teal),
             BottomNavigationBarItem(
             icon: Icon(Icons.design_services),
             // ignore: deprecated_member_use
             title:Text("خدماتي"),
             backgroundColor: Color(0xFF4682B4)),
             BottomNavigationBarItem(
             icon: Icon(Icons.arrow_right_alt),
             // ignore: deprecated_member_use
             title:Text("دليلي"),
             backgroundColor: Colors.cyan),
             BottomNavigationBarItem(
             icon: Icon(Icons.announcement),
             // ignore: deprecated_member_use
             title:Text("استفسارات"),
             backgroundColor: Colors.pink
             ),
             BottomNavigationBarItem(
             icon: Icon(Icons.assignment_late_rounded),
             // ignore: deprecated_member_use
             title:Text("عن البلدية"),
             backgroundColor: Colors.green 
             
               ),
         ],
         
       ),
       ),
     ),
     );  
       
  
    
  }
}



