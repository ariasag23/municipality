import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class EditeProfile extends StatefulWidget {
  State<StatefulWidget> createState() {
    return EditeProfileState();
  }
}

class EditeProfileState extends State<EditeProfile> {
  bool _secureText = true;
  String id = "";
  String fullName = "";
  String email = "";
  String water = "";
  String jawwal = "";
  bool isSingIn = false;
  final GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _fullNamecontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _jawwalcontroller = TextEditingController();
  TextEditingController _passwordcontroller = TextEditingController();

  void getFrom() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    id = prefs.getString("id");
    fullName = prefs.getString("fullName");
    email = prefs.getString("email");
    water = prefs.getString("water");
    jawwal = prefs.getString("jawwal");
    if (fullName != null) {
      setState(() {
        id = prefs.getString("id");
        print(id);
        fullName = prefs.getString("fullName");
        email = prefs.getString("email");
        water = prefs.getString("water");
        jawwal = prefs.getString("jawwal");
        isSingIn = true;
      });
    }
  }

  @override
  void initState() {
    getFrom();
    super.initState();
  }

  void _editData() async {
    var url = "https://gazaapp.000webhostapp.com/edit.php/${id}";

    var response = await http.post(Uri.parse(url), body: {
      "fullName": _fullNamecontroller.text,
      "email": _emailcontroller.text,
      "password": _passwordcontroller.text,
      "water": water,
      "jawwal": _jawwalcontroller.text
    });
    if (response.statusCode == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("تم العملية بنجاح"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا حاول مرة اخرى"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }

    //onEditedAccount();
    //print(_adresseController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF4682B4),
          title: Center(
            child: Text(
              "تعديل البيانات",
              style: TextStyle(
                color: Colors.white,
                fontSize: 26,
              ),
            ),
          ),
          leading:
              // action button
              IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        endDrawer: MyDrawer(),
        body: Form(
          key: _formKey,
          child: Directionality(
              textDirection: TextDirection.rtl,
              child: SafeArea(
                  child: Container(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 30, right: 30.0),
                      child: ListView(
                        children: <Widget>[
                          TextFormField(
                            autocorrect: true,
                            decoration: InputDecoration(
                              hintText: isSingIn ? fullName : 'الاسم',
                              suffixIcon: Icon(Icons.account_circle_outlined),
                            ),
                            controller: _fullNamecontroller,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: TextFormField(
                              autocorrect: true,
                              decoration: InputDecoration(
                                hintText:
                                    isSingIn ? email : 'البريد الالكتروني',
                                suffixIcon: Icon(Icons.email_outlined),
                              ),
                              controller: _emailcontroller,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: TextFormField(
                              enabled: false,
                              autocorrect: true,
                              decoration: InputDecoration(
                                hintText: isSingIn ? water : 'المياه',
                                suffixIcon: Icon(Icons.local_drink_outlined),
                              ),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(
                                top: 10.0,
                              ),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                autocorrect: true,
                                decoration: InputDecoration(
                                  hintText: isSingIn ? jawwal : 'الهاتف',
                                  suffixIcon:
                                      Icon(Icons.phone_android_outlined),
                                ),
                                controller: _jawwalcontroller,
                              )),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: TextFormField(
                              maxLength: 12,
                              obscureText: _secureText,
                              autocorrect: true,
                              decoration: InputDecoration(
                                hintText: 'كلمة السر',
                                suffixIcon: IconButton(
                                  icon: Icon(_secureText
                                      ? Icons.visibility_off_outlined
                                      : Icons.visibility_outlined),
                                  onPressed: () {
                                    setState(() {
                                      _secureText = !_secureText;
                                    });
                                  },
                                ),
                              ),
                              controller: _passwordcontroller,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: FlatButton(
                              child: Text(
                                'حفظ التعديلات',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              onPressed: () {
                                _editData();
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: FlatButton(
                              child: Text(
                                'ادارة الحسابات',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              onPressed: () {},
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: TextField(
                                keyboardType: TextInputType.number,
                                autocorrect: true,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'رقم الهوية',
                                )),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 10.0,
                            ),
                            child: FlatButton(
                              child: Text(
                                'بيانات متعلقة برقم الهوية',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              onPressed: () {},
                            ),
                          ),
                        ],
                      )))),
        ));
  }
}
