import 'package:flutter/material.dart';



class  Centres extends StatefulWidget {
  State <StatefulWidget> createState()
  {
    return CentresState();
  }
}

class CentresState extends State<Centres>  {
  @override
  Widget build(BuildContext context) { 
    return Directionality(
      
      textDirection: TextDirection.rtl,
     child :  
    Scaffold(
          body :   ListView(
          children: <Widget>[
          Container(
              margin: EdgeInsets.only(top :100,left: 30.0 ,right: 30.0),
              child: RaisedButton(
                child: Text('مركز الاطراف الصناعية',style: TextStyle(fontSize: 24),),
                color: Colors.cyan,
                textColor: Colors.white,
                 shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                onPressed: () {},
              ),
          ),
           Container(
              margin: EdgeInsets.only(top :10,left: 30.0 ,right: 30.0),
              child: RaisedButton(
                child: Text('مركز رشاد الشوا الثقافي',style: TextStyle(fontSize: 24),),
                color: Colors.cyan,
                textColor: Colors.white,
                 shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                onPressed: () {},
              ),
          ),
          Container(
              margin: EdgeInsets.only(top :10,left: 30.0 ,right: 30.0),
              child: RaisedButton(
                child: Text('مركز اسماد الطفولة',style: TextStyle(fontSize: 24),),
                color: Colors.cyan,
                textColor: Colors.white,
                 shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                onPressed: () {},
              ),
          ),
          Container(
              margin: EdgeInsets.only(top :10,left: 30.0 ,right: 30.0),
              child: RaisedButton(
                child: Text('المكتبة العامة',style: TextStyle(fontSize: 24),),
                color: Colors.cyan,
                textColor: Colors.white,
                 shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                onPressed: () {},
              ),
          ),
          Container(
              margin: EdgeInsets.only(top :10,left: 30.0 ,right: 30.0),
              child: RaisedButton(
                child: Text('مراكز  هولست الثقافي',style: TextStyle(fontSize: 24),),
                color: Colors.cyan,
                textColor: Colors.white,
                 shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                onPressed: () {},
              ),
          ),
           Container(
              margin: EdgeInsets.only(top :10,left: 30.0 ,right: 30.0),
              child: RaisedButton(
                child: Text('قرية الفنون والحرف',style: TextStyle(fontSize: 24),),
                color: Colors.cyan,
                textColor: Colors.white,
                 shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0))),
                onPressed: () {},
              ),
          ),

          ],
          ),
      
    ),
    );
  }
}