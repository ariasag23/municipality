import 'package:flutter/material.dart';

import 'complaint.dart';
import 'myComplaint.dart';


class  Inquiries extends StatefulWidget {
  State <StatefulWidget> createState()
  {
    return InquiriesState();
  }
}

class InquiriesState extends State<Inquiries>  {
  @override
  Widget build(BuildContext context) { 
    return Scaffold(
      appBar: AppBar(
     backgroundColor: Colors.pink,

   title: Text("شكاوي واستفسارات",style: TextStyle(color: Colors.white,fontSize: 26,)
        , textAlign: TextAlign.center), 
      
          leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
     ),
            
      ),


      body:GridView(
          padding: EdgeInsets.only(right: 10.0, left: 10.0, top: 100.0),
         
          children: <Widget>[
             Card(
               color: Colors.pink,
              child: InkWell(
             onTap: () {
               Navigator.push(context,MaterialPageRoute(builder: (context)=>Complaint()));
             },   
             splashColor: Colors.pink.shade300,
             child: Center(
               child:Column (
                 mainAxisSize:  MainAxisSize.min,
                 children: <Widget>[
                   Text("تقديم شكوى", style: new TextStyle(color: Colors.white,fontSize:24),),
                   Icon(Icons.assignment_turned_in_outlined,size:50.0,color:Colors.white),
                   
                 ],
                 ),
               ), 
             ),

              ),
                Card(
               color: Colors.pink,
              child: InkWell(
             onTap: () {
               Navigator.push(context,MaterialPageRoute(builder: (context)=>MyComplaint()));
             },   
             splashColor: Colors.pink.shade300,
             child: Center(
               child:Column (
                 mainAxisSize:  MainAxisSize.min,
                 children: <Widget>[
                   Text("عرض الشكاوي \n والردود", style: new TextStyle(color: Colors.white,fontSize:24),),
                   Icon(Icons.article_outlined,size:50.0,color:Colors.white),
                   
                 ],
                 ),
               ), 
             ),

              ),
              
               
          ],
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            
            childAspectRatio: 2 / 2
             )
         ),
      
    );
  }
  }