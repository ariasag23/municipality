import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'drawer.dart';

import 'package:http/http.dart' as http;

class MyComplaintdata {
  String fullName;
  String title;
  String calendar;

  MyComplaintdata({this.fullName, this.title, this.calendar});

  factory MyComplaintdata.fromJson(Map<String, dynamic> json) {
    return MyComplaintdata(
      fullName: json['fullName'],
      title: json['title'],
      calendar: json['calendar'],
    );
  }
}

class MyComplaint extends StatefulWidget {
  State<StatefulWidget> createState() {
    return MyComplaintState();
  }
}

class MyComplaintState extends State<MyComplaint> {
  String id;
  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id = prefs.getString("id");
    });
  }

  var url = "https://gazaapp.000webhostapp.com/myComplaint.php";

  // ignore: missing_return
  Future<List<MyComplaintdata>> allMyComplaint() async {
    var data = {'id_user': id};
    print(data);
    var response = await http.post(Uri.parse(url), body: json.encode(data));
    print(response.body);
    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();

      List<MyComplaintdata> studentList = items.map<MyComplaintdata>((json) {
        return MyComplaintdata.fromJson(json);
      }).toList();

      return studentList;
    } else {
      print('Failed to load data from Server.');
    }
  }

  @override
  void initState() {
    getFromSharedPreferences();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: Center(
          child: Text(
            "شكاواي",
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
            ),
          ),
        ),
        leading:
            // action button
            IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      endDrawer: MyDrawer(),
      body: Directionality(
          textDirection: TextDirection.rtl,
          child: FutureBuilder<List<MyComplaintdata>>(
              future: allMyComplaint(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(child: CircularProgressIndicator());
                return ListView(
                  children: snapshot.data
                      .map((data) => Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              children: <Widget>[
                                Row(children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top: 10.0, left: 0.0, right: 10.0),
                                    child: Image.asset(
                                      "assets/googlemaps.png",
                                      width: 40.0,
                                      height: 40.0,
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(
                                          top: 10.0, right: 30.0),
                                      child: Text(data.title.toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16))),
                                ]),
                                Row(children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top: 10.0, left: 0.0, right: 10.0),
                                    child: Image.asset(
                                      "assets/info.png",
                                      width: 40.0,
                                      height: 40.0,
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(
                                          top: 10.0, right: 30.0),
                                      child: Text('لم يتم الرد',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16))),
                                ]),
                                Row(children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top: 10.0, left: 0.0, right: 10.0),
                                    child: Image.asset(
                                      "assets/schedule.png",
                                      width: 40.0,
                                      height: 40.0,
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(
                                          top: 10.0, right: 30.0),
                                      child: Text(data.calendar.toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16))),
                                ]),
                              ],
                            ),
                          ))
                      .toList(),
                );
              })),
    );
  }
}
