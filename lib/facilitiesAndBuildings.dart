import 'package:flutter/material.dart';
import 'drawer.dart';
class FacilitiesAndBuildings extends StatefulWidget{

 @override

 State<StatefulWidget> createState() {

   // TODO: implement createState 

   return FacilitiesAndBuildingsState();

 }

 
}

class FacilitiesAndBuildingsState extends State<FacilitiesAndBuildings>{

  List<bool>listExpans=List();
  List<bool>list=List();
 
  @override

 void initState() {

   // TODO: implement initState

   super.initState();

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
  
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
   
   listExpans.add(false);

   listExpans.add(false);

   listExpans.add(false);
 }

 @override

 Widget build(BuildContext context) {

   // TODO: implement build

   return 
    Scaffold(
     appBar: AppBar(
       backgroundColor: Colors.green,
       title: Center ( child : Text("مرافق و مباني البلدية", )),
       
        leading: 
            // action button
            IconButton(
              icon: Icon( Icons.arrow_back_ios_outlined ),
              onPressed: () {
                Navigator.pop(context);
               },
            ),
      
      ),
      endDrawer:MyDrawer( 
      ),
       
       
      

     body:  Directionality(
      
      textDirection: TextDirection.rtl,
     child :  
      ListView(

       children : <Widget>[
          Container(

         alignment: Alignment.center,

         child: Column(

           children: <Widget>[
             Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child : Card(
                 color : Colors.white,
                 margin:EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Padding(padding: EdgeInsets.all(8),child:Center(child: Text('مباني البلدية الادارية',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w500),),),),
               ),
               
             ),
               Container(
           
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
          child: Theme(
            
            data: Theme.of(context).copyWith(cardColor: Colors.green),
            child: 
            

             ExpansionPanelList(

               children : <ExpansionPanel>[

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Padding(
                       padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0,0.0),
                       child :
                      ListTile(

                       title: Text('1. مجمع البلدية الرئيس',style: TextStyle(color : Colors.white, fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[
 
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يقع قرب ميدان فلسطين وسط مدينة غزة, وههو مجمع يضم عدد من مباني وادارات البلدية الرئيسة ومنها :'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* رئاسة البلدية : وهو مبني تاريخي يعتبر احد معالم مدينة غزة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* الادارة العامة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* المشتريات والعقود'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* الشؤون المالية'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* الشؤون القانونية'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* التنظيم والمساحة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2832200'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[0],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('2. مبنى الهندسة والتخطيط (البلدية القديمة)',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يقع في شارع عمر المختار في المنطقة ما بين ميدان فلسطين والشجاعية, ويضم مكاتب الادارة العامة للهندسة والتخطيط, ويعتبر احد المعالم التاريخية لمدينة غزة اذ تم انشائة بالعالم 1933 كمقر لبلدية غزة وتم هدمه جزئيا في العدوان على غزة عام 2008 وقامت البلدية باعادة بنائه كمان كان حفاظا على قيمته التاريخية'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),
                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2832200  داخلي 2008'),),
                          ),

                       ],

                     ),

                   ),

                   isExpanded: listExpans[1],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('3. مبنى الجباية العامة',style: TextStyle(color : Colors.white, fontSize: 16)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يقع في منقطة الرمال قرب مفترق السريا وبجوار محكمة غزة الشرعية, ويضم مكاتب الادارية العامة للصحة والبيئة, اضافة لمكاتب الجباية التابعة للادارة العامة للشؤن المالية.'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),
                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2861460'),),
                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[2],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('4. كراج البلدية',style: TextStyle(color: Colors.white, fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يضم مكاتب الادارية العامة للخدمات, وكراج البلدية ومبيت سيارات البلدية, ويقع في شارع الوحدة بجوار مكتبة البلدية العامة وسط مدينة غزة.'),),

                         ),
                          Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2865455'),),

                         ),
                        
                       ],

                     ),

                   ),

                   isExpanded: listExpans[3],

                   canTapOnHeader: true,

                 ),

             
 
               ],

               expansionCallback:(panelIndex, isExpanded){

                 setState(() {

                   listExpans[panelIndex] = !isExpanded;

                 });

               },

               animationDuration : kThemeAnimationDuration,

             ),
          ),
               ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child : Card(
                 color : Colors.white,
                 margin:EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Padding(padding: EdgeInsets.all(8),child:Center(child: Text('مرافق البلدية',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w500),),),),
               ),
               
             ),

              Container(
           
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
          child: Theme(
            
            data: Theme.of(context).copyWith(cardColor: Colors.green),
            child: 
            

             ExpansionPanelList(

               children : <ExpansionPanel>[

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Padding(
                       padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0,0.0),
                       child :
                      ListTile(

                       title: Text('1. ملعب اليرموك',style: TextStyle(color : Colors.white, fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[
 
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يعد من اقدم الملاعب في قطاع غزة, انشئ عام 1960 واعيد افتتاحه عام 2009, يضم مكاتب قسم الانشطة الرياضية, ويقع في شارع الوحدة قرب المكتبة العامة.'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف : 2882772'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* الادارة العامة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* المشتريات والعقود'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* الشؤون المالية'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* الشؤون القانونية'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('* التنظيم والمساحة'),),

                         ),
                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2832200'),),

                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[4],

                   canTapOnHeader: true,

                 ),

                 ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('2. مبنى الهندسة والتخطيط (البلدية القديمة)',style: TextStyle(color : Colors.white,fontSize: 18)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يقع في شارع عمر المختار في المنطقة ما بين ميدان فلسطين والشجاعية, ويضم مكاتب الادارة العامة للهندسة والتخطيط, ويعتبر احد المعالم التاريخية لمدينة غزة اذ تم انشائة بالعالم 1933 كمقر لبلدية غزة وتم هدمه جزئيا في العدوان على غزة عام 2008 وقامت البلدية باعادة بنائه كمان كان حفاظا على قيمته التاريخية'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),
                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2832200  داخلي 2008'),),
                          ),

                       ],

                     ),

                   ),

                   isExpanded: listExpans[5],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('3. مبنى الجباية العامة',style: TextStyle(color : Colors.white, fontSize: 16)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(15, 0, 15, 15),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يقع في منقطة الرمال قرب مفترق السريا وبجوار محكمة غزة الشرعية, ويضم مكاتب الادارية العامة للصحة والبيئة, اضافة لمكاتب الجباية التابعة للادارة العامة للشؤن المالية.'),),

                         ),
                          Card(
                          color : Colors.white,
                           margin:EdgeInsets.fromLTRB(0, 0, 0, 10),
                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2861460'),),
                         ),
                       ],

                     ),

                   ),

                   isExpanded: listExpans[6],

                   canTapOnHeader: true,

                 ),

                  ExpansionPanel(

 
                   headerBuilder:(context, isExpanded){

                     return Container(
                       padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0,0.0),
                       child :
                      ListTile(
                
                       title: Text('4. كراج البلدية',style: TextStyle(color: Colors.white, fontSize: 18,)),
                      ),
                     );

                   },

                   body: Padding(

                     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                     child: ListBody(

                       children: <Widget>[

                         Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('يضم مكاتب الادارية العامة للخدمات, وكراج البلدية ومبيت سيارات البلدية, ويقع في شارع الوحدة بجوار مكتبة البلدية العامة وسط مدينة غزة.'),),

                         ),
                          Card(
                          color: Colors.white,
                           margin:EdgeInsets.fromLTRB(16, 0, 16, 10),

                           child: Padding(padding: EdgeInsets.all(8),child: Text('هاتف رقم : 2865455'),),

                         ),
                        
                       ],

                     ),

                   ),

                   isExpanded: listExpans[7],

                   canTapOnHeader: true,

                 ),

             
 
               ],

               expansionCallback:(panelIndex, isExpanded){

                 setState(() {

                   listExpans[panelIndex] = !isExpanded;

                 });

               },

               animationDuration : kThemeAnimationDuration,

             ),
          ),
               ),
           ],

         ),

       ),
       ],
     )
    ),
   );

 
 }

 
}

