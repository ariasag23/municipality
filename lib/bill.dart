import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:aa/drawer.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Billdata {
  int water;
  String name;
  String dateaccount;
  int consumequantity;
  int pricewater;
  int cleanliness;
  int rats;
  int sanitation;
  int arrears;
  int readcounter;
  int total;

  Billdata(
      {this.water,
      this.name,
      this.dateaccount,
      this.consumequantity,
      this.pricewater,
      this.cleanliness,
      this.rats,
      this.sanitation,
      this.arrears,
      this.readcounter,
      this.total});

  factory Billdata.fromJson(Map<String, dynamic> json) {
    return Billdata(
      water: json['water'],
      name: json['name'],
      dateaccount: json['dateaccount'],
      consumequantity: json['consumequantity'],
      pricewater: json['pricewater'],
      cleanliness: json['cleanliness'],
      rats: json['rats'],
      sanitation: json['sanitation'],
      arrears: json['arrears'],
      readcounter: json['readcounter'],
      total: json['total'],
    );
  }
}

class Bill extends StatefulWidget {
  State<StatefulWidget> createState() {
    return BillState();
  }
}

class BillState extends State<Bill> {
  bool isSingIn = false;
  String billWater;
  Future getFrom() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    billWater = prefs.getString("water");

    if (billWater != null) {
      setState(() {
        billWater = prefs.getString("water");
        isSingIn = true;
      });
    }
    return billWater;
  }

  @override
  void initState() {
    getFrom();
    super.initState();
  }

  var url = 'https://gazaapp.000webhostapp.com/bill.php';

  // ignore: missing_return
  Future<List<Billdata>> allBill() async {
    var data = {'water': billWater};
    print(data);
    var response = await http.post(Uri.parse(url), body: json.encode(data));
    print(response.body);
    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();

      List<Billdata> studentList = items.map<Billdata>((json) {
        return Billdata.fromJson(json);
      }).toList();

      return studentList;
    } else {
      print('Failed to load data from Server.');
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget dividerLine = //
        Padding(
      padding: const EdgeInsets.only(
        top: 8,
        left: 16,
        right: 16,
      ),
      child: Container(
        child: Divider(color: Colors.blue),
        height: 0,
      ),
    );

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF4682B4),
          title: Center(
            child: Text(
              "فاتورتي",
              style: TextStyle(
                color: Colors.white,
                fontSize: 26,
              ),
            ),
          ),
          leading:
              // action button
              IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        endDrawer: MyDrawer(),
        body: Directionality(
            textDirection: TextDirection.rtl,
            child: FutureBuilder<List<Billdata>>(
                future: allBill(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());
                  return ListView(
                    children: snapshot.data
                        .map(
                          (data) => Column(
                            children: <Widget>[
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  //
                                  //
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        " رقم الاشتراك :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  //
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.water.toString(),
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                  //
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  //
                                  //
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "اسم المشترك :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.name,
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                  //
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "تاريخ الاحتساب :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.dateaccount,
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "كمية الاستهلاك :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.consumequantity.toString() +
                                                  'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "ثمن المياه :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.pricewater.toString() +
                                                  'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "نظافة :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  //

                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.cleanliness.toString() +
                                                  'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "فئران :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.rats.toString() + 'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "متأخرات :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.arrears.toString() + 'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "قراءة العداد :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.readcounter.toString() +
                                                  'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //
                              dividerLine,
                              //
                              Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 16,
                                    ),
                                    child: Container(
                                      child: Text(
                                        "إجمالي الفاتورة :",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 24,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8,
                                      left: 16,
                                      right: 8,
                                    ),
                                    child: Container(
                                      child: isSingIn
                                          ? Text(
                                              data.total.toString() + 'شيكل',
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          : Text(""),
                                    ),
                                  )
                                ],
                              ),
                              //

                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 8,
                                  left: 16,
                                  right: 16,
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  height: 48,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Color(0xFF4682B4),
                                      borderRadius: BorderRadius.circular(0.0)),
                                  child: FlatButton(
                                    onPressed: () {},
                                    child: Text(
                                      'فواتير سابقة',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 8,
                                  left: 16,
                                  right: 16,
                                ),
                                child: Container(
                                  child: Text(
                                    "بإمكانك دفع الفاتورة الشهرية من خلال  مراكز تحصيل البلدية - معارض شركة زياد مرتجى واخوانه مرتجى - قنوات  الالكترونية - تطبيق بنكي",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                        .toList(),
                  );
                })));
  }
}
