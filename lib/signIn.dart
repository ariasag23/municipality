import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'signUp.dart';
import 'forgetPassword.dart';
import 'package:aa/main.dart';
import 'api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  // For CircularProgressIndicator.
  bool visible = false;
  String apiUrl = Api.url;
  String id = '';
  String fullName = "";
  String email = "";
  String water = "";
  String jawwal = "";

  final GlobalKey<FormState> _formKey = GlobalKey();

  TextEditingController _emailcontroller = TextEditingController();

  TextEditingController _passwordcontroller = TextEditingController();

  void savePref(String id, String fullName, String email, String water,
      String jawwal) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("id", id);
    await prefs.setString("fullName", fullName);
    await prefs.setString("email", email);
    await prefs.setString("water", water);
    await prefs.setString("jawwal", jawwal);
  }

  @override
  void initState() {
    super.initState();
  }

  login() async {
    final response = await http
        .post(Uri.parse("https://gazaapp.000webhostapp.com/signIn.php"), body: {
      "email": _emailcontroller.text,
      "password": _passwordcontroller.text
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 1) {
      savePref(data['id'], data['fullName'], data['email'], data['water'],
          data['jawwal']);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MyApp(),
        ),
      );
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("الايميل او كلمة السر خطاء"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
      print(pesan);
    }
  }

  /* Future login() async {
    var url = "https://gazaapp.000webhostapp.com/signIn.php";
    var response = await http.post(url, body: {
      "email": _emailcontroller.text,
      "password": _passwordcontroller.text,
    });
    var data = jsonDecode(response.body);
    
    
    if (data == "Success") {
      Navigator.push(context, MaterialPageRoute(builder: (context)=>MyApp(),),);
    } else {
      showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: new Text("الايميل او كلمة السر خطاء"),
        actions: <Widget>[
          FlatButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    });
    }
  }
  */
//

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading:
// action button
            IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: ListView(children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(15.0, 20.0, 10.0, .0),
                      child: Text('بلدية',
                          style: TextStyle(
                              fontSize: 80.0, fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(16.0, 100.0, 40.0, 0.0),
                      child: Text('غزة',
                          style: TextStyle(
                              fontSize: 80.0, fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(210.0, 125.0, 220.0, 0.0),
                      child: Text('.',
                          style: TextStyle(
                              fontSize: 80.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.green)),
                    )
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            decoration: InputDecoration(
                                labelText: 'الايميل',
                                labelStyle: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                                suffixIcon: Icon(
                                  Icons.email_outlined,
                                  color: Colors.green,
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.green))),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'ادخل الايميل';
                              }

                              if (!RegExp(
                                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                  .hasMatch(value)) {
                                {
                                  return 'الايميل غير صحيح';
                                }
                              } else {
                                return null;
                              }
                            },
                            onSaved: (value) {},
                            controller: _emailcontroller,
                          ),
                          SizedBox(height: 20.0),
                          TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            decoration: InputDecoration(
                                labelText: 'كلمة السر',
                                labelStyle: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                                suffixIcon: Icon(
                                  Icons.lock_outlined,
                                  color: Colors.green,
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.green))),
                            obscureText: true,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'ادخل كلمة السر';
                              }
                              if (value.isEmpty || value.length <= 5) {
                                return 'كلمة السر قصيرة';
                              }
                              return null;
                            },
                            onSaved: (value) {},
                            controller: _passwordcontroller,
                          ),
                          SizedBox(height: 5.0),
                          Container(
                            alignment: Alignment(1.0, 0.0),
                            padding: EdgeInsets.only(top: 10.0, left: 20.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ForgetPassword()));
                              },
                              child: Text(
                                'استرجاع كلمة السر',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Montserrat',
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          ),
                          SizedBox(height: 40.0),
                          Container(
                            height: 40.0,
                            child: Material(
                              borderRadius: BorderRadius.circular(20.0),
                              shadowColor: Colors.greenAccent,
                              color: Colors.green,
                              elevation: 7.0,
                              child: GestureDetector(
                                onTap: () {
                                  if (_formKey.currentState.validate()) {
                                    login();
                                  } else {
                                    return null;
                                  }
                                },
                                child: Center(
                                  child: Text(
                                    'تسجيل دخول',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat'),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                        ],
                      ))),
              SizedBox(height: 15.0),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'مشترك جديد ؟',
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                    SizedBox(width: 5.0),
                    InkWell(
                      onTap: () async {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SignUp()));
                      },
                      child: Text(
                        'تسجيل ',
                        style: TextStyle(
                            color: Colors.green,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  ])
            ],
          ),
        ]),
      ),
    );
  }
}
