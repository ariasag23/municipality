import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'drawer.dart';

class Care extends StatefulWidget {
  @override
  _CareState createState() => _CareState();
}

class _CareState extends State<Care> {
  bool checkBoxValue = false;
  File _file;

  final GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _namecontroller = TextEditingController();
  TextEditingController _jawwalcontroller = TextEditingController();
  TextEditingController _addresscontroller = TextEditingController();
  TextEditingController _titlecontroller = TextEditingController();
  TextEditingController _detailscontroller = TextEditingController();

  Future getFile() async {
    File file = await FilePicker.getFile(type: FileType.CUSTOM);
    print(file);
    setState(() {
      if (file != null) {
        _file = file;
      } else {
        print("No File Selected.");
      }
    });
  }

  Future uploadCare() async {
    final uri = Uri.parse("https://gazaapp.000webhostapp.com/care.php");
    var request = http.MultipartRequest('POST', uri);
    request.fields['name'] = _namecontroller.text;
    request.fields['jawwal'] = _jawwalcontroller.text;
    request.fields['address'] = _addresscontroller.text;
    request.fields['title'] = _titlecontroller.text;
    request.fields['details'] = _detailscontroller.text;
    var pic = await http.MultipartFile.fromPath("file", _file.path);
    request.files.add(pic);
    var response = await request.send();

    if (response.statusCode == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("تم العملية بنجاح"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("عذرا حاول مرة اخرى"),
              actions: <Widget>[
                FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF4682B4),
          title: Center(
            child: Text(
              "احتضان ورعاية",
              style: TextStyle(
                color: Colors.white,
                fontSize: 26,
              ),
            ),
          ),
          leading:
              // action button
              IconButton(
            icon: Icon(Icons.arrow_back_ios_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        endDrawer: MyDrawer(),
        body: Form(
          key: _formKey,
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: ListView(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(top: 50.0, right: 12.0),
                    child: Text(
                      "لديك موهبة , فكرة , مبادرة , تحتاج الي رعاية او احتضان , ارسلها لنا وسيتم التواصل معكم",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    )),
                Container(
                  margin: EdgeInsets.only(top: 30.0, left: 12.0, right: 12.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "الاسم",
                      fillColor: Colors.white,
                    ),
                    controller: _namecontroller,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12.0, right: 12.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: "رقم الجوال",
                      fillColor: Colors.white,
                    ),
                    controller: _jawwalcontroller,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12.0, right: 12.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "العنوان",
                      fillColor: Colors.white,
                    ),
                    controller: _addresscontroller,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12.0, right: 12.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "عنوان الفكرة",
                      fillColor: Colors.white,
                    ),
                    controller: _titlecontroller,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12.0, right: 12.0),
                  child: TextFormField(
                    maxLines: 3,
                    decoration: new InputDecoration(
                      labelText: "التفاصيل",
                      fillColor: Colors.white,
                    ),
                    controller: _detailscontroller,
                  ),
                ),
                InkWell(
                  onTap: () {
                    getFile();
                  },
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 10.0, right: 20.0),
                        child: Image.asset(
                          "assets/pdf.png",
                          width: 70.0,
                          height: 70.0,
                        ),
                      ),
                      Center(
                          child: _file == null
                              ? Text(
                                  "الرجاء تحميل الملف ",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                )
                              : Text(
                                  "تم رفع الملف",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF4682B4)),
                                )),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20, left: 30.0, right: 30.0),
                  child: RaisedButton(
                    child: Text(
                      'ارسل',
                      style: TextStyle(fontSize: 24),
                    ),
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    onPressed: () {
                      uploadCare();
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
