-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 21 مارس 2021 الساعة 16:21
-- إصدار الخادم: 10.3.16-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id15697828_gazaapp`
--

-- --------------------------------------------------------

--
-- بنية الجدول `bill`
--

CREATE TABLE `bill` (
  `id` int(11) NOT NULL,
  `water` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateaccount` date NOT NULL,
  `consumequantity` int(11) NOT NULL,
  `pricewater` int(11) NOT NULL,
  `cleanliness` int(11) NOT NULL,
  `rats` int(11) NOT NULL,
  `sanitation` int(11) NOT NULL,
  `arrears` int(11) NOT NULL,
  `readcounter` int(11) NOT NULL,
  `currentinvoice` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `bill`
--

INSERT INTO `bill` (`id`, `water`, `name`, `dateaccount`, `consumequantity`, `pricewater`, `cleanliness`, `rats`, `sanitation`, `arrears`, `readcounter`, `currentinvoice`, `total`) VALUES
(1, 6788, 'احمد عيد سليم غياضة', '2020-04-16', 0, 6, 13, 1, 25, 3731, 0, 39, 3770),
(2, 369, 'عيد سليم غياضة', '2020-05-16', 50, 6, 13, 5, 0, 4098, 4098, 4098, 7000),
(3, 649, 'رامي عيد غياضة', '2020-06-16', 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- بنية الجدول `care`
--

CREATE TABLE `care` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `jawwal` int(11) NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `care`
--

INSERT INTO `care` (`id`, `name`, `jawwal`, `address`, `title`, `details`, `file`) VALUES
(1, 'احمد عيد غياضة ', 59299247, 'النصر ', 'اصلاح صرف صحي ', 'تدقيق والتتبع ومراجعة المشروع ', 'امتحان نهائي.pdf');

-- --------------------------------------------------------

--
-- بنية الجدول `complaintd`
--

CREATE TABLE `complaintd` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `iduser` int(11) NOT NULL,
  `typecomplaintsd` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `calendar` date NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `complaintd`
--

INSERT INTO `complaintd` (`id`, `title`, `details`, `iduser`, `typecomplaintsd`, `area`, `location`, `img`, `calendar`, `id_user`) VALUES
(1, 'صرف صحي ', 'تدقيق والنظر في ملفات صرف صحي ', 407018100, 'صرف صحي', 'النصر', '', 'image_picker7103476462597878166.jpg', '2021-01-19', 24),
(2, 'انارة', 'توفير انارة في شوارع', 407018100, 'انارة', 'النصر', '', 'image_picker512926561104122962.jpg', '2021-01-19', 24),
(4, 'تكسير الشوارع', 'المراجعة وتتبع العملية', 407018100, 'طرق', 'النصر', '', 'image_picker6304852636438040240.jpg', '2021-01-30', 24);

-- --------------------------------------------------------

--
-- بنية الجدول `counterWater`
--

CREATE TABLE `counterWater` (
  `id` int(11) NOT NULL,
  `numbercounter` int(11) NOT NULL,
  `img` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `datecounter` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `counterWater`
--

INSERT INTO `counterWater` (`id`, `numbercounter`, `img`, `datecounter`) VALUES
(6, 45689, 'image_picker6790345604600972191.jpg', '2021-01-18'),
(7, 45689, 'image_picker6790345604600972191.jpg', '2021-01-18'),
(8, 78956, 'image_picker8820553835698887410.jpg', '2021-01-18'),
(9, 78956, 'image_picker8820553835698887410.jpg', '2021-01-18'),
(10, 22222, 'image_picker4306759392475858418.jpg', '2021-01-18');

-- --------------------------------------------------------

--
-- بنية الجدول `craft`
--

CREATE TABLE `craft` (
  `id` int(11) NOT NULL,
  `namecraft` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nameusercraft` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `building` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `jawwal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `craft`
--

INSERT INTO `craft` (`id`, `namecraft`, `nameusercraft`, `building`, `city`, `phone`, `jawwal`) VALUES
(1, 'سوبر ماركت ', 'ابو شنب', 1, 1779, 0, 0),
(2, 'سوبر ماركت', 'احمد غياضة', 2, 1889, 0, 0);

-- --------------------------------------------------------

--
-- بنية الجدول `deals`
--

CREATE TABLE `deals` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `future` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `deals`
--

INSERT INTO `deals` (`id`, `number`, `year`, `name`, `sender`, `future`) VALUES
(1, 6788, 2021, 'ترخيص بناء', 'شؤون قانونية', 'ارشفة الكترونية');

-- --------------------------------------------------------

--
-- بنية الجدول `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `nameEmployees` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `circle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `jawwal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `employees`
--

INSERT INTO `employees` (`id`, `nameEmployees`, `circle`, `phone`, `jawwal`) VALUES
(1, 'احمد عيد غياضة', '12345', 0, 592990247);

-- --------------------------------------------------------

--
-- بنية الجدول `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `img`) VALUES
(1, 'قرية الفنون و الحرف تحيي يوم التراث الفلسطيني بمهرجان تراثي\r\n', 'افتتح رئيس بلدية غزة د.يحيى السراج مهرجان التراث الفلسطيني في مركز رشاد الشوا الثقافي', 'assets/news.jpg'),
(2, 'تنفيذ مبادرة لتنظيف وتجميل حديقة الصداقة في حي التفاح', 'اطلقت بلدية غزة وجمعية الخريجات الجامعيات مبادرة مجتمعية لتنظيف وتجميل حديقة الصداقة في حي تفاح شرق مدينة غزة\r\n', 'assets/news3.jpg'),
(3, 'عقد برنامج تدريبي للعاملات في مشروع توعي بطلبة المدارس\r\n', 'عقدت بلدية غزة برنامج تدريبي للمثقفات البيئيات العاملات في مشروع تعزيز المعرفة والوعي البيئي عند طلبة المدارس والمجتمع المحلي في مدينة غزة', 'assets/news4.jpg'),
(4, 'لجان الاحياء تؤكد دعمها ومساندتها لبلدية غزة', 'اكد رؤساء لجان في مدينة غزة دعمهم ومساندتهم لبلدية غزةغزة في ظل الظروف الصعبة التي تمر بها المدينة والبلدية بسبب جائحة كورونا والحصار المفروض على قطاع غزة\r\n', 'assets/news2.jpg');

-- --------------------------------------------------------

--
-- بنية الجدول `tableWater`
--

CREATE TABLE `tableWater` (
  `id` int(11) NOT NULL,
  `place` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timer` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `tableWater`
--

INSERT INTO `tableWater` (`id`, `place`, `area`, `timer`) VALUES
(1, 'مدينة العودة', 'ابراج المخابرات - حي المرابطين - ابراج المقوسي', '6:00 صباحا - 5:00 مساء'),
(2, 'النصر', 'شارع المشتل - ابراج فيروز غرب الايطالي', '8:00 صباحا - 10:00 مساء'),
(3, 'الشيخ رضوان', 'قسيمة 65 - قسيمة 66 - قسيمة 69', '8:00 صباحا - 8:00 مساء'),
(4, 'البلاخية', 'البلاخية', '6:00 صباحا - 8:00 مساء'),
(5, 'معسكر الشاطىء', 'بلوك رقم 3 - بلوك رقم 4 - بلوك رقم 6 - بلوك رقم 7', '6:00 صباحا - 7:00 مساء'),
(6, 'الدرج', 'الصحابة', '4:00 صباحا - 5:00 مساء'),
(7, 'البلدة القديمة ', 'ميدان فلسطين - مسجد العمري - شارع المحكمة', '6:00 صباحا - 4:00 مساء'),
(8, 'الشجاعية - التركمان', 'شارع بغداد - سوق الخضرة - سوق الجمعة - شارع الطواحين', '3:00 صباحا - 8:00 مساء');

-- --------------------------------------------------------

--
-- بنية الجدول `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `water` int(11) NOT NULL,
  `jawwal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- إرجاع أو استيراد بيانات الجدول `users`
--

INSERT INTO `users` (`id`, `fullName`, `email`, `password`, `water`, `jawwal`) VALUES
(2, 'رامي ‏غياضة ‏', 'rami@gmail.com', 'omar--s', 649, 596431976),
(3, 'عيد غياضة', 'eid@gmail.com', 'omar--s', 369, 593654836),
(4, 'فاتن غياضة ', 'faten@gmail.com', 'omar--s', 538, 593658473),
(24, 'احمد عيد غياضة ', 'ariasag23@gmail.com', 'omar--s', 6788, 592990247);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `care`
--
ALTER TABLE `care`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaintd`
--
ALTER TABLE `complaintd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counterWater`
--
ALTER TABLE `counterWater`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `craft`
--
ALTER TABLE `craft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deals`
--
ALTER TABLE `deals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tableWater`
--
ALTER TABLE `tableWater`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `care`
--
ALTER TABLE `care`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `complaintd`
--
ALTER TABLE `complaintd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `counterWater`
--
ALTER TABLE `counterWater`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `craft`
--
ALTER TABLE `craft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deals`
--
ALTER TABLE `deals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tableWater`
--
ALTER TABLE `tableWater`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
